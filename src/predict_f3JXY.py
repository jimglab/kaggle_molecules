#!/usr/bin/env python3
import sys, os, argparse

# custom libs
import funcs as ff
import sfuncs as sff
import funcs_train as fft

# graphics 
import numpy as np
import matplotlib.pyplot as plt

sys.stdout.flush()
sys.stderr.flush()
#sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-frac-inp',
type=float,
default=0.1,
help='N of the sub-sample of all the molecules.',
)
parser.add_argument(
'-np', '--nproc',
type=int,
default=0,
help='number of processors.',
)
parser.add_argument(
'-dir-src',
default='./data',
help='directory where kaggle data is stored.',
)
parser.add_argument(
'-fname-desc', '--fname_descriptors',
default='./data/postproc/struct_descriptors_ii.csv',
help='CSV of the spatial descriptors.',
)
parser.add_argument(
'-fname-more',
default='./test.h5',
help='HDF5 for all other calculated features.',
)
parser.add_argument(
'-type',
default='none',
help='which type of couling to train.',
)
parser.add_argument(
'-out-prefix',
default='',
help='filename prefix for output paths.',
)
pa = parser.parse_args()


dirs_src = {
    'dir_src'                   : pa.dir_src,
    'fname_inp_spatial_desc'    : pa.fname_descriptors,
    'fname_inp_more'                : pa.fname_more,
}

#dirs_src = {'dir_src':'/media/sergio/EVO970/Kaggle/molecules/kaggle_molecules/data',
#                'fname_spatial_descriptors':'/media/sergio/EVO970/Kaggle/molecules/kaggle_molecules/data/struct_descriptors_ii.csv',
#                'fname_more':'/media/sergio/EVO970/Kaggle/molecules/kaggle_molecules/data/interaction_desc.h5'}


t3j = fft.jcoupling_trainer(
    paths_src = dirs_src,
    frac_crossval = 0.25,
    nproc = pa.nproc,
    frac_inp = pa.frac_inp,
    )


t3j._load__spatial_descriptors_and_mulliken()


#--- read features from the hdf5 (Couloumb, Lennard-Jones, diedral angles)
t3j._load__interaction_energies()


# - normalize L-J energies
# - normalize Coulomb
t3j._collect_features()


#--- TODO: separate into train/cross-validation sets
t3j._prepare__train_and_cv()

t3j._load_3jxy_features()

if pa.type == '3J':

    #--- test some model parameters
    #    dict([ ('n_estimators', 8192),  ('num_leaves', 1023), ('reg_alpha', 0.1), ('reg_lambda', 0.3) ]),
    #    dict([ ('n_estimators', 16384), ('num_leaves', 511),  ('reg_alpha', 0.1), ('reg_lambda', 0.3) ]),
    #    dict([ ('n_estimators', 16384), ('num_leaves', 1023), ('reg_alpha', 0.1), ('reg_lambda', 0.3) ]),  # log3.txt
    list_param = [
        dict([ ('n_estimators', 16384), ('num_leaves', 1023), ('reg_alpha', 0.3), ('reg_lambda', 0.6) ]),  # log3.txt
        dict([ ('n_estimators', 32768), ('num_leaves', 511),  ('reg_alpha', 0.1), ('reg_lambda', 0.3) ]),
        dict([ ('n_estimators', 32768), ('num_leaves', 1023), ('reg_alpha', 0.1), ('reg_lambda', 0.3) ]),
        ]
    #---
    #list_param = [
    #    (32768, 511), (32768, 1023),
    #    ]
    #    #(4096, 511), (4096, 1023), (8192, 511), (8192, 1023), (16384, 511), (16384, 1023), (32768, 511), (32768, 1023),
    #    #]
   
    fname_out_pkl = pa.out_prefix + '__perf.pkl' if pa.out_prefix else ''
    performs = fft.test_several_lightgbm(
        list_param = list_param,
        train = t3j.train_3j,
        crossval = t3j.cross_val_3j,
        nproc = pa.nproc,
        fname_out = fname_out_pkl,
        )

elif pa.type == 'all':

    pass
    # TODO: train for the whole dataset

else:

    print('\n [!] not processing anything because the specified type (%s) is unknown.\n' % pa.type)


#EOF
