import pickle
import re

def load_obj(file_d = './file.net', verbose=True):
    f = open(file_d, 'br')
    n = pickle.load(f)
    f.close()
    if verbose:
        print(' - Objeto', type(n), os.path.basename(file_d),'leído de disco.')
    return n

def dump_obj(n, file_d = './file.net', verbose=True):
    f = open(file_d, 'bw')
    pickle.dump(n, f)
    f.close()
    if verbose:
        print(' - Objeto', type(n), os.path.basename(file_d), 'salvado en disco.')
        
    return None


class DB_dict(dict):

    def __init__(self, *argsv, **argsd):
        super().__init__(*argsv, **argsd)

        self.__items2attr__()

        return None


    def __items2attr__(self):
        for item in self.keys():
            if type(item) is str:
                item_str = re.sub(r'[^\w]', '_', item)
                if item_str[0].isdecimal():
                    item_str = 'n' + item_str
                    
                if item_str not in dir(self):
                    self.__setattr__(item_str, self[item])

            
    def __setitem__(self, item, value):
        r = super().__setitem__(item, value)
        self.__items2attr__()
        return r

    def __str__(self):
        return self.__repr__()

    
    def __repr__(self):
        keys = self.keys()
        
        
        max_k = max_t = 0
        k_v = []
        t_v = []
        a_v = []
        
        for k in keys:
            item = self[k]
            item_type = type(item)
            
            if 'shape' in dir(item):
                add_info = 'S:' + str( item.shape )
                
            elif item_type in [list, tuple]:
                add_info = 'L: ' + str( len(item) )

            elif item_type in [int, str, float]:
                add_info = 'V: ' + str( item )
            else:
                add_info = '---'
                

            k_v.append(str(k))
            t_v.append(str(item_type).split("'")[1])
            a_v.append(add_info)
            max_k = max(max_k, len(k_v[-1]))
            max_t = max(max_t, len(t_v[-1]))
            

        s = 'DB_dict summary:'
        if len(keys):
            for k, t, a in zip(k_v,t_v,a_v):
                s += ('\n - {:'+str(max_k)+'s}  {:'+str(max_t)+'}  {}').format(k, t, a)
        else:            
            s += '\n - No keys Found, DB is Empty.'
            
        return s
