#!/usr/bin/env python3
import numpy as np
import pandas as pd
import random
from scipy.linalg import norm

import matplotlib.pyplot as plt
import seaborn as sns
import os, sys, argparse

import multiprocessing as mp

import time
from scipy.spatial.distance import cdist
from collections import namedtuple
import sfuncs as sff


dir_src = sys.argv[1]
assert os.path.isdir(dir_src)

#---
bt = sys.argv[2] #'3JHC'  # '3JHN'  #'3JHH'
assert bt in ('3JHH', '3JHN', '3JHC')
print('\n --> running for atom-type: ', bt)


#---
#ds_trn, ds_tst, struct_dict = load_dss()
ds_struct   = sff.load_ds(f'{dir_src}/structures.csv')
ds_trn      = sff.load_ds(f'{dir_src}/train.csv')
#ds_tst      = sff.load_ds('./data/test.csv')

struct_dict = sff.make_struct_dict(ds_struct)


#---
print(' Filtrando', bt)
ds_trn_f = sff.ds_filter(ds_trn, ds_trn['type'] == bt)


print('Cantidad de datos en ds_trn_f: {}'.format(ds_trn_f['id'].shape[0]))


#ret_d_trn = sff.calc_descriptors_3JXY(ds_trn, struct_dict, 
ret_d_trn = sff.calc_descriptors_3JXY(ds_trn_f, struct_dict, 
    idx_v=(0, -1), 
    )
#dump_obj(ret_d_trn, 'desc_3JHH_trn.pickle')


#EOF
