#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import random
from scipy.linalg import norm

import matplotlib.pyplot as plt
import seaborn as sns
import os, sys, argparse

import multiprocessing as mp

#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-sample',
type=int,
default=0,
help='N of the sub-sample of all the molecules',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
default='',
help='output .csv',
)
parser.add_argument(
'-ds', '--dir-src',
type=str,
default='',
help='directory of the input dataset',
)
parser.add_argument(
'-w', '--workdir',
type=str,
default='/work/kaggle/molecules/src',
help='absolute dir of the library',
)
parser.add_argument(
'-np', '--nproc',
type=int,
default=0,
help='number of processors',
)
pa = parser.parse_args()

#--- custom libs
if pa.workdir:
    sys.path.insert(0, pa.workdir)
import sfuncs as sff
import funcs as ff

assert os.path.isdir(pa.dir_src)


mm = ff.molecule_mgr(f"{pa.dir_src}/structures.csv")

ds_train    = sff.load_ds(f'{pa.dir_src}/train.csv')

struct_dict = sff.make_struct_dict(mm.m_structs)

# NOTE: para eta=(1/0.5)**2 no parece haber un esparcimiento razonable en los
# histogramas (son muy localizados) de los valores de los descriptores.
params_g2ang = {
0: {
    'lambda' : 1.,
    'eta'    : (1/1)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

1: {
    'lambda' : -1.,
    'eta'    : (1/1)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

2: {
    'lambda' : 1.,
    'eta'    : (1/1.5)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

3: {
    'lambda' : -1.,
    'eta'    : (1/1.5)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

4: {
    'lambda' : 1.,
    'eta'    : (1/2.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

5: {
    'lambda' : -1.,
    'eta'    : (1/2.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

6: {
    'lambda' : 1.,
    'eta'    : (1/3.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

7: {
    'lambda' : -1.,
    'eta'    : (1/3.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

8: {
    'lambda' : 1.,
    'eta'    : (1/4.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

9: {
    'lambda' : -1.,
    'eta'    : (1/4.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

10: {
    'lambda' : 1.,
    'eta'    : (1/5.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

11: {
    'lambda' : -1.,
    'eta'    : (1/5.)**2,
    'psi'    : 4,
    'Rc'     : 10,
    },

12: {
    'lambda' : 1.,
    'eta'    : (1/1)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

13: {
    'lambda' : -1.,
    'eta'    : (1/1)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

14: {
    'lambda' : 1.,
    'eta'    : (1/1.5)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

15: {
    'lambda' : -1.,
    'eta'    : (1/1.5)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

16: {
    'lambda' : 1.,
    'eta'    : (1/2.)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

17: {
    'lambda' : -1.,
    'eta'    : (1/2.)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

18: {
    'lambda' : 1.,
    'eta'    : (1/3.)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },

19: {
    'lambda' : -1.,
    'eta'    : (1/3.)**2,
    'psi'    : 4,
    'Rc'     : 6,
    },
}

# NOTE: as "rule of thumb", I'm using twice the observed max value 
# of distance between atoms, for Rc.
params_g2rad = {
0: {
    'eta' : (1/0.5)**2,
    'Rc'  : 10.0,
    'Rs'  : 0.8,
    },

1: {
    'eta' : (1/0.5)**2,
    'Rc'  : 10.0,
    'Rs'  : 1.4,
    },

2: {
    'eta' : (1/0.5)**2,
    'Rc'  : 10.0,
    'Rs'  : 2.1,
    },

3: {
    'eta' : (1/0.8)**2,
    'Rc'  : 10.0,
    'Rs'  : 0.8,
    },

4: {
    'eta' : (1/0.8)**2,
    'Rc'  : 10.0,
    'Rs'  : 1.4,
    },

5: {
    'eta' : (1/0.8)**2,
    'Rc'  : 10.0,
    'Rs'  : 2.1,
    },

6: {
    'eta' : (1/0.5)**2,
    'Rc'  : 10.0,
    'Rs'  : 2.6,
    },

7: {
    'eta' : (1/0.7)**2,
    'Rc'  : 10.0,
    'Rs'  : 3.6,
    },

8: {
    'eta' : (1/0.7)**2,
    'Rc'  : 10.0,
    'Rs'  : 4.2,
    },

9: {
    'eta' : (1/0.7)**2,
    'Rc'  : 10.0,
    'Rs'  : 5.0,
    },
}

molecule_list = random.sample(struct_dict.keys(), pa.sample) if pa.sample>0 else list(struct_dict.keys())
nmols = len(molecule_list)

print(' [*] running for %d molecules...' % nmols)

nproc = mp.cpu_count() if pa.nproc<=0 else pa.nproc # total number of processors
print(' [*] running in %d processors...' % nproc)
pool = mp.Pool(nproc)

mlist_bd = ff.equi_bounds(0, len(molecule_list), nproc)

params = {"g2ang": params_g2ang, "g2rad": params_g2rad}
args = [ (struct_dict, molecule_list[mlist_bd[i]:mlist_bd[i+1]+1], params) for i in range(len(mlist_bd)-1) ]

df_list = pool.starmap(ff.build_df_wDescriptors, args)

#df = ff.build_df_wDescriptors(
#    struct_dict, 
#    molecule_list, 
#    {"g2ang": params_g2ang, "g2rad": params_g2rad},
#    )


df = pd.concat(df_list, sort=False)
del df_list

# clean
df.reset_index(inplace=True)
df.drop("index", axis=1, inplace=True)

# save to storage
df.to_csv(pa.fname_out, sep=';')

print(' ---> FINISHED!')

#EOF
