#!/usr/bin/env python3

import sys, os
import funcs_train as fft


dir_src = sys.argv[1]
os.path.isdir(dir_src)

fname_out = sys.argv[2]


feat_mgr = fft.feature_builder(
    fdata = {
        "dir_src"   : dir_src,
        "fname_out" : fname_out,
        },
    )

feat_mgr.build_all()


#EOF
