# -*- coding: utf-8 -*-
"""
All implementations in this file a from Sergio Papadakis (@socom20 in GitLab).
"""

import os, sys
import pandas as pd
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pickle
import time
from scipy.spatial.distance import cdist
from collections import namedtuple

#import keras

def load_obj(file_d = './file.net', verbose=True):
    f = open(file_d, 'br')
    n = pickle.load(f)
    f.close()
    if verbose:
        print(' - Objeto', type(n), os.path.basename(file_d),'leído de disco.')
    return n

def dump_obj(n, file_d = './file.net', verbose=True):
    f = open(file_d, 'bw')
    pickle.dump(n, f)
    f.close()
    if verbose:
        print(' - Objeto', type(n), os.path.basename(file_d), 'salvado en disco.')




##def get_struct(ds_struct,  molecule_name='dsgdb9nsd_000001'):
##    f = np.argwhere(ds_struct['molecule_name']==molecule_name).T[0]
##    struct_v = ds_struct['pos'][f]
##    atom_v   = ds_struct['atom'][f]
##    return struct_v, atom_v



def get_struct(struct_dict,  molecule_name='dsgdb9nsd_000001'):
    return struct_dict[molecule_name]



def plot_molecule(struct_dict, molecule_name='dsgdb9nsd_000001', ds=None):
    struct_v, atom_v = get_struct(struct_dict, molecule_name)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    c_d = {'C':'k',
           'F':'y',
           'H':'c',
           'N':'g',
           'O':'b'}

    lb_v = []
    for (x,y,z), a in zip(struct_v, atom_v):
        if a not in lb_v:
            lb_v.append(a)
            lb = True
        else:
            lb = False
            
        ax.scatter([x],[y],[z], s=100, c=c_d[a], marker='o', edgecolors='k', label=a if lb else None)


    if ds is not None:

        # types of links
        bound_types_ls = {
            '1JHC': '-r', 
            '1JHN': ':r',
            '2JHC': '-g', 
            '2JHH': '-.k', 
            '2JHN': ':g', 
            '3JHC': '-b', 
            '3JHH': '-.c', 
            '3JHN': ':b'}

        f_ds = ds['molecule_name'] == molecule_name

        if len(f_ds) == 0:
            print('No se puede graficar vinculos, el ds no tiene el nombre de la molécula', file=sys.stderr )
        else:
            for idx_0, idx_1, tt in zip(ds['atom_index_0'][f_ds], ds['atom_index_1'][f_ds], ds['type'][f_ds]):
                a_0 = struct_v[idx_0]
                a_1 = struct_v[idx_1]
                
                ax.plot([a_0[0], a_1[0]],
                        [a_0[1], a_1[1]],
                        [a_0[2], a_1[2]], bound_types_ls[tt], label=tt, alpha=0.5)
        
    
    
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_title(molecule_name)

    ax.legend()
    #plt.show()
    #return None
    return fig, ax




def calc_atoms_inf(p, struct_v, atom_v):
    atom_d = 1/(1+np.square( struct_v - p ).sum(axis=-1))

    atom_inf = np.zeros(5)
    for a, d in zip(atom_v, atom_d):
        i_a = atm2idx[a]
        atom_inf[i_a] += d
        
    return atom_inf



def calc_bound_inf(i_ds, ds, struct_dict, n_steps=100, return_coupling=False):
    struct_v, atom_v = get_struct(struct_dict,  molecule_name=ds['molecule_name'][i_ds])


    p0 = struct_v[ds['atom_index_0'][i_ds]]
    p1 = struct_v[ds['atom_index_1'][i_ds]]

    t_v = np.linspace(0, 1, n_steps)

    ret_v = []
    for t in t_v:
        p = p0*(1+t) + p1*t 
        atom_inf = calc_atoms_inf(p, struct_v, atom_v)

        ret_v.append(atom_inf)

    if return_coupling:
        return np.array(ret_v), ds['scalar_coupling_constant'][i_ds]
    else:
        return np.array(ret_v)




def make_ds(ds, struct_dict, i_s=0, n=5000, n_steps=100):
    ds_d = {'input':[],
           'target':[]}

    if n == -1:
        i_e = ds['type'].shape[0]
    else:
        i_e = min(i_s+n, ds['type'].shape[0])

    
    for i_ds in range(i_s, i_e):
        i, t = calc_bound_inf(i_ds, ds, struct_dict, n_steps=n_steps, return_coupling=True)

        ds_d['input'].append(i)
        ds_d['target'].append(t)

    ds_d['input'] = np.array(ds_d['input'])
    ds_d['target'] = np.array(ds_d['target'])
    
    return ds_d




def build_model2(input_shape=(50,5), lr=1e-3):
    model = keras.Sequential()
    model.add( keras.layers.InputLayer(input_shape=input_shape) )
    
    model.add( keras.layers.Conv1D(128, 3) )
    model.add( keras.layers.MaxPool1D(2) )

    model.add( keras.layers.Conv1D(128, 3) )
    model.add( keras.layers.MaxPool1D(2) )

    model.add( keras.layers.Conv1D(256, 3) )
    

    model.add( keras.layers.Flatten() )
    for n_h in [1024, 128, 64]:
        model.add( keras.layers.Dense(128, activation='tanh') )
        
    model.add( keras.layers.Dense(1, activation='linear') )
    
    opt=keras.optimizers.Adam(lr)
    model.compile(opt, 'mse', metrics=['mae'] )
    
    return model


##def build_model(input_shape=(50,5), lr=1e-3):
##    model = keras.Sequential()
##    model.add( keras.layers.InputLayer(input_shape=input_shape) )
##    
##    model.add( keras.layers.Flatten() )
##
##    for n_h in [1024, 512, 128, 64]:
##        model.add( keras.layers.Dense(128, activation='tanh') )
##        
##    model.add( keras.layers.Dense(1, activation='linear') )
##    
##    opt=keras.optimizers.Adam(lr)
##    model.compile(opt, 'mse', metrics=['mae'] )
##    
##    return model


def build_model(input_shape=(50,5), n_hidden_v=[1024, 512, 512, 256, 128], lr=1e-3):
    model = keras.Sequential()
    model.add( keras.layers.InputLayer(input_shape=input_shape) )

    model.add( keras.layers.Flatten() )

    for n_h in n_hidden_v:
        model.add( keras.layers.Dense(128, activation='tanh') )

    model.add( keras.layers.Dense(1, activation='linear') )

    opt=keras.optimizers.Adam(lr)
    model.compile(opt, 'mse', metrics=['mae'] )

    return model



def make_struct_dict(ds_struct):
    from collections import OrderedDict
    print('Armando struct_dict', end='')
    struct_dict = OrderedDict()
    i = 0
    n = ds_struct['pos'].shape[0]
    while i < n:
        j = i
        while j<n and ds_struct['molecule_name'][i] == ds_struct['molecule_name'][j]:
            j += 1

        struct_dict[ ds_struct['molecule_name'][i] ] = (ds_struct['pos'][i:j], ds_struct['atom'][i:j])
        i = j

    print(' OK!')
    return struct_dict



def train(model, ds, batch_size=32, validation_split=0.2, n_epochs=1000, use_global_parameters=True):
    global hist_v
    global best_weights
    global best_val_mae
    
    if not use_global_parameters or 'hist_v' not in dir():
        hist_v = []

    if not use_global_parameters or 'best_val_mae' not in dir():
        best_val_mae = np.inf
    
    for i_epoch in range(n_epochs):
        h = model.fit(ds['input'], ds['target'], batch_size=batch_size, epochs=1, validation_split=validation_split, verbose=False)
        hist_v.append(h.history)
        print(' i_epoch={i_epoch:d} - loss_trn={loss[0]:5.03f} - mae_trn={mean_absolute_error[0]:5.03f}   loss_val={val_loss[0]:5.03f} - mae_val={val_mean_absolute_error[0]:5.03f}'.format(i_epoch=i_epoch, **h.history))


        if h.history['val_mean_absolute_error'][-1] < best_val_mae:
            print('Salvando pesos.')
            best_val_mae = h.history['val_mean_absolute_error']
            best_weights = model.get_weights()

    model.set_weights(best_weights)

    return None



def load_ds(filename):
    print('Cargando:', filename, '...', end='')
    
    ds = {}
    ds_pd = pd.read_csv(filename)

    cols = [c for c in ds_pd.columns if c not in 'xyz']
    for c in cols:
        ds[c] = ds_pd[c].values

    if 'x' in ds_pd.columns:
        ds['pos'] = ds_pd.iloc[:,3:].values
        
    print(' OK!')
    return ds



def ds_filter(ds, filtro):
    ds_ret = {}
    for k in ds.keys():
        if type(ds[k]) is np.ndarray:
            ds_ret[k] = ds[k][filtro]
            
        elif type(ds[k]) is list and len(ds[k])>0 and type(ds[k][0]) is np.ndarray:
            ds_ret[k] = [ ds[k][i_ds][filtro] for i_ds in range(len(ds[k]))]
            
        else:
            ds_ret[k] = ds[k]

    return ds_ret


#++++++++++++++++++++++++++++++++++++++++++++++
#
# Diedral ("torsion") angles
#


def cos_xy(X,Y):
    """ Calculates cos beetwen X, Y vectors
        """
    return np.dot(X, Y) / (np.linalg.norm(X) * np.linalg.norm(Y))



def feat_3JHH(H0, H1, C0, C1, C0H0_norm, C1H1_norm):
    """ Calculates cos()
    """

    C0C1 = C1 - C0
    C0H0 = H0 - C0
    C1H1 = H1 - C1

    C0C1_norm = norm(C0C1)
    C0H0_norm = norm(C0H0)
    C1H1_norm = norm(C1H1)

    n1 = np.cross(C0C1, C0H0)
    n2 = np.cross(C0C1, C1H1)

    if norm(n1) == 0 or norm(n2) == 0: # both planes are coplanar (*)
        cos0 = 1
    else:
        cos0 =  np.dot(n1, n2) / (norm(n1) * norm(n2))        # Cos( Angle_between_planes )

    cos1 =  np.dot(C0H0, C0C1) / (C0H0_norm * C0C1_norm)  # Cos( Angle_H0 )
    cos2 = -np.dot(C1H1, C0C1) / (C1H1_norm * C0C1_norm)  # Cos( Angle_H1 )

    # (*): this happens because there exist at least 3 atoms that are co-linear.
    
    return cos0, cos1, cos2, C0C1_norm



def calc_descriptors_3JXY(ds, struct_dict, idx_v=(0, -1), th_name=None):
    
    C0C1_dist_v = []
    cos_coupling_v = []
    H0_neighbours_dist_v = []
    H1_neighbours_dist_v = []
    neighbours_atoms_v   = []
    H0_nn_dist, H1_nn_dist = [], []
    atom_H0nn, atom_H1nn = [], []
    
    if idx_v[1] == -1:
        idx_v = (idx_v[0], ds['molecule_name'].shape[0])

    t0 = time.time()
    for i_c in range(*idx_v):
        molecule_name = ds['molecule_name'][i_c]

        idx_H0 = ds['atom_index_0'][i_c]    # (*)
        idx_H1 = ds['atom_index_1'][i_c]    # (*)

        # pos_v: Array posicion para todos los atomos
        # atm_v: Array de tipo de atomo
        pos_v, atm_v = struct_dict[molecule_name]

        # Filtro para los no H (*)
        assert atm_v[idx_H0] == 'H'
        f_noH = (atm_v != 'H')
        f_noH[idx_H1] = False # this is because H1 is not necessarily an hidrogen

        pos_noH_v = pos_v[f_noH]

        
        d_H0_v = cdist(pos_v[idx_H0:idx_H0+1], pos_noH_v)[0]
        d_H1_v = cdist(pos_v[idx_H1:idx_H1+1], pos_noH_v)[0]

        i_C0 = np.argmin(d_H0_v)
        i_C1 = np.argmin(d_H1_v)

        H0 = pos_v[idx_H0]
        H1 = pos_v[idx_H1]
        C0 = pos_noH_v[ i_C0 ]
        C1 = pos_noH_v[ i_C1 ]

        # Ya tengo calculadas las 2 normas estas no tiene sentido calcularlas en feat_3JHH
        C0H0_norm = d_H0_v[i_C0]
        C1H1_norm = d_H1_v[i_C1]
        cos0, cos1, cos2, C0C1_norm = feat_3JHH(H0, H1, C0, C1, C0H0_norm, C1H1_norm)

        # get the types of the nearest neighbors to H0 and H1 (*)
        atom_H0nn.append( atm_v[f_noH][i_C0] )
        atom_H1nn.append( atm_v[f_noH][i_C1] )

        C0C1_dist_v.append( C0C1_norm )
        cos_coupling_v.append( [cos0, cos1, cos2] )
        H0_neighbours_dist_v.append( d_H0_v )
        H1_neighbours_dist_v.append( d_H1_v )
        H0_nn_dist.append( C0H0_norm )
        H1_nn_dist.append( C1H1_norm )
        neighbours_atoms_v.append( atm_v[f_noH] )

    
    print(' Done {}, ELP={:0.2f}s'.format(idx_v, time.time()-t0))

    ret_dict = {
        'idx_v' : idx_v,
        'C0C1_dist_v': np.array(C0C1_dist_v),
        'cos_coupling_v': np.array(cos_coupling_v),
        'H0_neighbours_dist_v': np.array(H0_neighbours_dist_v),
        'H1_neighbours_dist_v': np.array(H1_neighbours_dist_v),
        'neighbours_atoms_v': np.array(neighbours_atoms_v),
        'H0_nn_dist' : np.array(H0_nn_dist),
        'H1_nn_dist' : np.array(H1_nn_dist),
        'atom_H0nn'  : np.array(atom_H0nn),
        'atom_H1nn'  : np.array(atom_H1nn),
        }

    if th_name is not None:
        global ret_d
        ret_d[th_name] = to_return

    # (*) : actually, the "H0" and "H1" refer to the atom involved in the coupling, which are
    #       not necessarily hidrogens. We call them H0, H1 because initially we were thinking
    #       this routine for 3JHH, but actually turns out very generalizable for 3JXY and
    #       the notation H0/H1 is pretty handy!
        
    return ret_dict


def main1():

    atm2idx = {'H':0,'F':1,'C':2,'N':3,'O':4}
    bound_types_v = ['1JHC', '1JHN', '2JHC', '2JHH', '2JHN', '3JHC', '3JHH', '3JHN']
    
    ds_struct = load_ds('structures.csv')
    ds_trn    = load_ds('train.csv')
    ds_tst    = load_ds('test.csv')

    struct_dict = make_struct_dict(ds_struct)

##    plot_molecule(struct_dict,  molecule_name='dsgdb9nsd_000010', ds=ds_trn)

    print('filtrando')
    ds_trn = ds_filter(ds_trn, ds_trn['type']==bound_types_v[0])
    ds_tst = ds_filter(ds_tst, ds_tst['type']==bound_types_v[0])


# ['C', 'H', 'N', 'O', 'F']
if __name__ == '__main__':

    main1()




#EOF 
