#!/usr/bin/env python3
import numpy as np
import pandas as pd
import random
from scipy.linalg import norm

import matplotlib.pyplot as plt
import seaborn as sns
import os, sys, argparse

import multiprocessing as mp

import time
from scipy.spatial.distance import cdist
from collections import namedtuple
import sfuncs as sff
import funcs as ff


dir_src = sys.argv[1]
assert os.path.isdir(dir_src)


#---
bt = sys.argv[2] #'3JHC'  # '3JHN'  #'3JHH'
assert bt in ('3JHH', '3JHN', '3JHC')
print('\n --> running for atom-type: ', bt)

#---
ds_struct   = sff.load_ds(f'{dir_src}/structures.csv')
ds_trn      = sff.load_ds(f'{dir_src}/train.csv')


struct_dict = sff.make_struct_dict(ds_struct)

#---
print(' Filtrando', bt)
ds_trn_f = sff.ds_filter(ds_trn, ds_trn['type'] == bt)


print('Cantidad de datos en ds_trn_f: {}'.format(ds_trn_f['id'].shape[0]))


print(' > Interaccion entre pares acoplados... ', end='')
lj_energy_coupled = ff.lj_energies_between_coupled(ds_trn_f, struct_dict, 
    sigma = 5.0, 
    idx_v = (-1,)
    )
print('OK!\n')

print(' > Interaccion entre c/ atomo con todos los demas atomos de la molecula... ', end='')
lj_dict = ff.lj_energies_between_all(ds_trn_f, struct_dict,
    sigma = 5.0,
    idx_v = (-1,),
    )
print('OK!\n')




#EOF
