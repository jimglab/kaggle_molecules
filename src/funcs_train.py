#!/usr/bin/env python3
"""
Library of routines related to training: from feature organization to 
training itself.
"""
import os, sys, argparse
import multiprocessing as mp
import time
from collections import namedtuple

# numerical
import numpy as np
import pandas as pd
import random
from scipy.linalg import norm
from scipy.spatial.distance import cdist

# graphics
import matplotlib.pyplot as plt
import seaborn as sns

# custom libs
import sfuncs as sff
import funcs as ff
from aux_fun import DB_dict

# machine learning
import sklearn.cluster
import sklearn
import sklearn.preprocessing
import sklearn.decomposition
from sklearn.model_selection import train_test_split
import lightgbm

# deep learning
from keras.models import Sequential 
from keras.layers.core import Dense, Activation
from keras.utils import np_utils


#+++++++++++++++++++++++++++++++++++++++
#
# UNIFICATION OF FEATURES
#


class feature_builder:

    def __init__(self, fdata, frac_validation=0.75, frac_total=1.0):

        #self.inp__mulliken = mulliken
        #self.inp__int_energy = int_energy
        #self.inp__spatial = spatial

        self.fdata = fdata
        self.frac_validation = frac_validation
        self.frac_total = frac_total


    def build_all(self, ):
        """
        - coupling/bond type
        - original index from the Kaggle "train" dataset
        - coupling constant
        - spatial descriptors (*0)
        - mulliken charge (*1)
        + Coulomb interaction energy
        + Lennard-Jones energy (*2)
        + relative distances
        + diedral angles
        - Karplus fit (*3)

        (*0) : esto ya lo tenemos en un .csv.
        (*1) : la red entrenada .pkl solo sera necesario cargarla
               cuando submitamos predicciones del test datasset.
        (*2) : need to normalize it before using it for training.
        (*3) : the fit will be made on our defined "train" dataset, and
               tested against our cross-validation dataset.
        """

        #--- Inputs
        dir_src     = self.fdata["dir_src"]
        ds_struct   = sff.load_ds(f'{dir_src}/structures.csv')
        ds_trn      = sff.load_ds(f'{dir_src}/train.csv')
        struct_dict = sff.make_struct_dict(ds_struct)

        print(' > Calculating: ')

        #-- append: diedral cosine and other structural features
        print(' [*] diedral angles... ')
        cc3j        = (ds_trn['type'] == '3JHH') | (ds_trn['type'] == '3JHN') | (ds_trn['type'] == '3JHC')
        ds_trn_f    = sff.ds_filter(ds_trn, cc3j)
        struct_feat = self._feat__molecule_structure(ds_trn_f, struct_dict)
        # q me queden los indices de estos 3JXY
        idx_3JXY    = cc3j.nonzero()[0]
        print(' [+] OK!\n')
        del ds_trn_f

        # 2J indexes
        cc2j      = (ds_trn['type'] == '2JHH')
        cc2j     |= (ds_trn['type'] == '2JHN')
        cc2j     |= (ds_trn['type'] == '2JHC')
        idx_2JXY  = cc2j.nonzero()[0]

        # 1J indexes
        cc1j      = (ds_trn['type'] == '1JHN')
        cc1j     |= (ds_trn['type'] == '1JHC')
        idx_1JXY  = cc1j.nonzero()[0]

        #-- append: relative distances
        print(' [*] relative distances... ')
        r = self._feat__rel_distances(ds_trn, struct_dict)
        print(' [+] OK!\n')


        #-- append: Lennard-Jones energies
        # energy between coupled pairs
        print(' [*] L-J energies between coupled atoms... ')#, end='')
        lj_ene      = ff.lj_energies_between_coupled(ds_trn, struct_dict, sigma=5., rel_dist=r, idx_v=(-1,))
        print(' [+] OK!')
        # between eacch coupld-atom (index 0 and 1) and all others
        print(' [*] L-J energies of coupled atoms against all the rest... ') #, end='')
        lj_all_dict = ff.lj_energies_between_all(ds_trn, struct_dict, sigma=5., idx_v=(-1,))
        print(' [+] OK!\n')


        #-- append: Coulomb interaction energy
        print(' [*] Coulomb energies... ') #, end='')
        coulomb_ene = ff.coulomb_between_coupled(ds_trn, struct_dict, rel_dist=r, idx_v=(-1,))
        print(' [+] OK!\n')


        #--- UNIFY && SAVE
        df_out = pd.DataFrame({
            'rel_dist'            : r,
            'lj_energy'           : lj_ene,
            'lj_sum_energy_0'     : lj_all_dict['sum_energy_0'],
            'lj_sum_energy_1'     : lj_all_dict['sum_energy_1'],
            'lj_mean_energy_0'    : lj_all_dict['mean_energy_0'],
            'lj_mean_energy_1'    : lj_all_dict['mean_energy_1'],
            'coulomb_interaction' : coulomb_ene,
            }
        )

        f5 = pd.HDFStore(self.fdata['fname_out'], mode='w')

        #--- features for all types of coupling
        df_out.astype('float32').to_hdf(f5, key='features/all', )


        #--- features just for 3JXY couplings
        pd.DataFrame({
            'diedral_cos'        : struct_feat['diedral_cos'],
            'cos1_between_bonds' : struct_feat['cos1_between_bonds'],
            'cos2_between_bonds' : struct_feat['cos2_between_bonds'],
            'distance_C0C1'      : struct_feat['distance_C0C1'],
            'H0_nn_dist'         : struct_feat['H0_nn_dist'],
            'H1_nn_dist'         : struct_feat['H1_nn_dist'],
            'Z_H0nn'             : struct_feat['Z_H0nn'],
            'Z_H1nn'             : struct_feat['Z_H1nn'],
            'idx_3JXY'           : idx_3JXY,
            }).astype('float32').to_hdf(f5, key='features/f3JXY')


        #--- features just for 2JXY couplings
        pd.DataFrame({
            "idx_2JXY"  : idx_2JXY,
            }).to_hdf(f5, key='features/f2JXY')


        #--- features just for 1JXY couplings
        pd.DataFrame({
            "idx_1JXY"  : idx_1JXY,
            }).to_hdf(f5, key='features/f1JXY')


        # TODO: save more metadata in `f5`.
        f5.close()



    def _feat__molecule_structure(self, ds, struct_dict):

        # Ok, these atomic numbers are extracted from:
        # https://en.wikipedia.org/wiki/List_of_chemical_elements
        zdict = {
        'H': 1,
        'C': 6,
        'N': 7,
        'O': 8,
        'F': 9,
        }
   
        ret_d_trn = sff.calc_descriptors_3JXY(
            ds,
            struct_dict, 
            idx_v = (0, -1), 
            )

        # cosine of the diedral angle
        diedral_cos        = ret_d_trn["cos_coupling_v"][:,0]
        cos1_between_bonds = ret_d_trn["cos_coupling_v"][:,1]
        cos2_between_bonds = ret_d_trn["cos_coupling_v"][:,2]

        # atomic numbers (Z) of the nearest neighbor of each atom of the coupling
        Z_H0nn  = list(map(lambda atype: zdict[atype], ret_d_trn["atom_H0nn"]))
        Z_H1nn  = list(map(lambda atype: zdict[atype], ret_d_trn["atom_H1nn"]))

        # the cardinal of the train set == the diedral cosines we calculated
        assert diedral_cos.size == ds["atom_index_0"].size

        # TODO: we could actually extract other cosines calculated by Sergio.
        o = {
        'diedral_cos'        : diedral_cos,
        'cos1_between_bonds' : cos1_between_bonds,
        'cos2_between_bonds' : cos2_between_bonds,
        'distance_C0C1'      : ret_d_trn['C0C1_dist_v'],
        'H0_nn_dist'         : ret_d_trn['H0_nn_dist'],
        'H1_nn_dist'         : ret_d_trn['H1_nn_dist'],
        'Z_H0nn'             : Z_H0nn,
        'Z_H1nn'             : Z_H1nn,
        }
        return o


    def _feat__rel_distances(self, ds, struct_dict):

        dist = ff.rel_distances(ds, struct_dict)

        return dist


    #def setUp_train_and_cv(self):
    #    """
    #    Setup the train and cross-validation datasets.
    #    """

    #    ---

    #    self.data = {
    #    "train"     : ---,
    #    "cv"        : ---,
    #    "idx_train" : ---,
    #    "idx_cv"    : ---,
    #    }




#+++++++++++++++++++++++++++++++++++++++
#
# TRAINING FOR 1JXY, 2JXY and 3JXY
#


class jcoupling_trainer:

    #--- TODO: read:
    # - train dst
    # - structures
    def __init__(self, paths_src, paths_dst, frac_crossval=0.25, nproc=1, frac_inp=1.):
        """
        paths_src     : dict of input paths
        frac_crossval : fraction of the [final] train set to use as cross validation data
        """
        
        #--- Inputs
        dir_src          = paths_src["dir_src"]
        self.ds_struct   = sff.load_ds(f'{dir_src}/structures.csv')
        self.struct_dict = sff.make_struct_dict(self.ds_struct)
        self.ds_trn      = sff.load_ds(f'{dir_src}/train.csv')

        self.n_train_orig = self.ds_trn['atom_index_0'].shape[0]
        self.rand_state   = random.getstate() # this is horrible list of seed numbers

        #--- slice
        if frac_inp < 1.:

            self.n_train        = int(frac_inp * self.n_train_orig)
            self.idx_train_orig = random.sample(np.arange(self.n_train_orig).tolist(), self.n_train)

            for knm, value in self.ds_trn.items():
                
                self.ds_trn[knm] = value[self.idx_train_orig]


        #--- selfing
        self.paths_src      = paths_src
        self.paths_dst      = paths_dst
        self.frac_crossval  = frac_crossval
        self.nproc          = nproc
        self.frac_inp       = frac_inp

        #---
        self.features = {
        'dim'  : {},
        'adim' : {},
        }



    def _load_3jxy_features(self,):
       
        print('\n [*] loading 3JXY features...')
        #idx_3jxy = np.array([ [self.idx_train_orig[i], i] for i in range(len(self.idx_train_orig)) \
        #    if self.idx_train_orig[i] in self.features['adim']['f3jxy__idx'] ], dtype=np.int64)
        _idx_3jxy        = list(set(self.idx_train_orig) & set(self.features['adim']['f3jxy__idx']))
        _df              = pd.DataFrame({"dumy": self.idx_train_orig})
        _df["idx"]       = _df.index.values
        #self.idx_ord_3j = _df.set_index("dumy", drop=False).loc[_idx_3jxy,"idx"].sort_values().values
        self.idx_ord_3j  = _df.set_index("dumy", drop=False).loc[_idx_3jxy,"idx"].values
        del _df

        # seleccionar los features grales para estos indices
        #df_common = self.features['adim']['common'].iloc[idx_3jxy[:,1],:]
        df_common = self.features['adim']['common'].iloc[self.idx_ord_3j,:]
        df_common.reset_index(drop=True, inplace=True)

        df_f3jxy  = self.features['adim']['f3jxy__interactions'].loc[_idx_3jxy,:]
        df_f3jxy.reset_index(drop=True, inplace=True)
        #df_f3jxy.drop("idx_3JXY", axis=1, inplace=True)

        coupling_const = self.features['adim']['coupling_constant'].iloc[self.idx_ord_3j,:]

        print(' [*] concatenating with common features...')
        # NOTE: we are assuming all features are of floating-point type !!
        df_feat = pd.concat(
            [ df_common, df_f3jxy ],
            axis = 1,
            ).astype('float32')

        #--- split
        print(' [*] spliteando usando una fraccion %.2f para test...' % self.frac_crossval)
        x_train, x_test, y_train, y_test = train_test_split(
            df_feat,
            coupling_const,
            test_size = self.frac_crossval,
            random_state = 0,
            )
        
        self.train_3j = DB_dict({
            'input'  : x_train,
            'target' : y_train
            })
        
        self.cross_val_3j  = DB_dict({  
            'input'  : x_test,
            'target' : y_test
            })

        print(' ---> 3JXY data:')
        print(' - x_train.shape :', x_train.shape)
        print(' - x_test.shape  :', x_test.shape)


    
     
    def _load__spatial_descriptors_and_mulliken(self,):
        """
        read spatial descriptors, mulliken charges && merge them with train dset
        """

        #--- spatial descriptors
        print(' [*] reading spatial descriptors... ')
        df = pd.read_csv(self.paths_src["fname_inp_spatial_desc"], sep=';')
        print('     [+] columns: ', df.columns)

        df.drop("Unnamed: 0", axis=1, inplace=True)

        df.head()

        #--- Mulliken charges
        print(' [*] reading Mulliken charges... ')
        fname_mulliken = "%s/mulliken_charges.csv" % self.paths_src["dir_src"]
        df_mulliken = pd.read_csv(fname_mulliken, sep=',')
        df_mulliken.head()

        df_merge = pd.merge(df, df_mulliken, 
            how="left", 
            on=["molecule_name", "atom_index"],
            )
        
        fnan = (~df_merge["mulliken_charge"].isna()).nonzero()[0].size / df_merge.shape[0]
        print(' [*] fraccion de filas con NaNs como resultado "bruto" del merge: ', fnan)

        df_merge.dropna(axis=0, how='any', subset=["mulliken_charge",], inplace=True)
        df_merge.reset_index(drop=True, inplace=True)

        #self.df_spatialDesc_and_Mulliken = df_merge
        self.features['dim'].update({
        'spatialDescriptors_and_Mulliken' : df_merge,
        })
    
    
    
    #--- TODO: read features from the hdf5 (Couloumb, Lennard-Jones, diedral angles)
    # x normalize L-J energies
    # x normalize Coulomb
    # - normalize diedral angles??
    def _load__interaction_energies(self,):

        fname_more = self.paths_src["fname_inp_more"]
        # features for all couplings
        df_all  = pd.read_hdf(fname_more, key='features/all', mode='r')
    
        #--- slice
        if self.frac_inp < 1.:

            df_all = df_all.iloc[self.idx_train_orig, :]
            df_all.reset_index(drop=True, inplace=True)


        # features only for 3JXY ones
        df_3jxy = pd.read_hdf(fname_more, key='features/f3JXY', mode='r')
        df_3jxy.set_index(df_3jxy['idx_3JXY'].values.astype(np.int64), inplace=True)

        # 2JXY
        df_2jxy = pd.read_hdf(fname_more, key='features/f2JXY', mode='r')

        # 1JXY
        df_1jxy = pd.read_hdf(fname_more, key='features/f1JXY', mode='r')

        self.features['dim'].update({
        'interact_all'  : df_all,
        'interact_3jxy' : df_3jxy,
        'interact_2jxy' : df_2jxy,
        'interact_1jxy' : df_1jxy,
        })
    
    

    #--- TODO: separate into train/cross-validation sets
    def _prepare__train_and_cv(self,):
        """ Prepare the train and cross-validation sets.
        """
        x_train, x_test, y_train, y_test = train_test_split(
            self.features['adim']['common'].values, 
            self.features['adim']['coupling_constant'].values[:,0], 
            test_size = self.frac_crossval, 
            random_state = 0,
            )

        #--- indexes for cross-validation set
        # NOTE: the 'self.idx_*' are indexes of self.ds_trn[key][:]
        self.idx_train, self.idx_crossval, _dummy, _dummy = train_test_split(
            np.arange(len(self.idx_train_orig)),
            self.features['adim']['coupling_constant'].values[:,0],
            test_size = self.frac_crossval,
            random_state = 0,
            )
        del _dummy
        
        print(' - x_train.shape :', x_train.shape)
        print(' - x_test.shape  :', x_test.shape)
        
        
        self.train = DB_dict({
            'input'  : x_train,
            'target' : y_train
        })
        
        self.cross_val  = DB_dict({  
            'input'  : x_test,
            'target' : y_test
        })



    # - normalize the train dataset:
    #    - spatial descriptors
    #    - L-J energies
    #    - Coulomb energies
    def _collect_features(self,):
        """
        Collect all the features. Note that we have features for:
        - all the couplings
        - specific type of couplings

        So we return a dict with keys:
        - common  : all features common to all coupling cases
        - coupling_constant : corresponding coupling constants
        - f3jxy__interactions : features only for 3JXY type of couplings.
        """

        #--- split
        df_features = pd.DataFrame()

        # backup atom types and drop it
        atom_types = self.features['dim']['spatialDescriptors_and_Mulliken']["atom"].values

        print('\n [*] normalize the spatial descriptors (and Mulliken charges) and *align* them with train data...')
        o__desc_and_mullik = self._build_and_normalize__desc_and_mullik()
        df_norm__desc_and_mullik = o__desc_and_mullik['df_norm']


        #--- normalize interaction energies
        print('\n [*] grab interaction-features, involving the whole train set...')
        o__more = self._normalize__interactions_all()
        df_norm__more = o__more['df_norm']


        #--- normalize 3JXY features
        print('\n [*] grab interaction-features, of only 3JXY couplings...')
        o__3jxy = self._normalize__3jxy()
        df_norm_3jxy = o__3jxy['df_norm']


        #--- unified features normalized
        print('\n [*] concatenate features for the whole train set...')
        df_norm = pd.concat(
            [df_norm__desc_and_mullik, df_norm__more,],
            axis = 1,
            )
        assert df_norm.shape[0] == df_norm__desc_and_mullik.shape[0] == df_norm__more.shape[0], \
            "\n [-] the concatenation wen't wrong!!\n"

        print('\n [*] DONE.')

        #---
        self.features['adim'].update({
            'common'              : df_norm,
            'coupling_constant'   : pd.DataFrame(self.ds_trn["scalar_coupling_constant"]),
            'f3jxy__interactions' : df_norm_3jxy,
            'f3jxy__idx'          : o__3jxy['idx'],
            'f2jxy__idx'          : self.features['dim']['interact_2jxy']['idx_2JXY'].values,
            'f1jxy__idx'          : self.features['dim']['interact_1jxy']['idx_1JXY'].values,
            })



    def _scatter_and_gather(self,):

        self.features['dim']['spatialDescriptors_and_Mulliken'].drop(["atom",], axis=1, inplace=True)

        self.features['dim']['spatialDescriptors_and_Mulliken'].set_index(["molecule_name", "atom_index"], inplace=True)

        colnames   = self.features['dim']['spatialDescriptors_and_Mulliken'].columns.values
        colnames_0 = [ _cnm + '_0' for _cnm in colnames ]
        colnames_1 = [ _cnm + '_1' for _cnm in colnames ]

        # size of train data
        ndata = self.ds_trn['atom_index_0'].size

        pool = mp.Pool(self.nproc)

        # build args for each processor
        bd_idx      = ff.equi_bounds(0, ndata-1, self.nproc)
        idx_atoms   = [ np.arange(bd_idx[i], bd_idx[i+1]) for i in range(bd_idx.size-1) ]
        mol_names   = [ self.ds_trn['molecule_name'][bd_idx[i]:bd_idx[i+1]] for i in range(self.nproc) ]
        IDXs_atom_0 = [ self.ds_trn['atom_index_0'][bd_idx[i]:bd_idx[i+1]] for i in range(self.nproc) ]
        IDXs_atom_1 = [ self.ds_trn['atom_index_1'][bd_idx[i]:bd_idx[i+1]] for i in range(self.nproc) ]

        # build the buffers for each processor
        dd = [ {} for _ in range(self.nproc) ]
        n_chunk = ff.equi_intervals(0, ndata-1, self.nproc) # size of each buffer
        assert all( n_chunk[i]==IDXs_atom_0[i].size for i in range(self.nproc) )

        for ip in range(self.nproc):
            dd[ip].update(dict([ (_cnm+'_0', np.nan*np.ones(n_chunk[ip])) for _cnm in colnames ]))
            dd[ip].update(dict([ (_cnm+'_1', np.nan*np.ones(n_chunk[ip])) for _cnm in colnames ]))

        
        args = [ (mol_names[ip], IDXs_atom_0[ip], IDXs_atom_1[ip], self.features['dim']['spatialDescriptors_and_Mulliken'], \
            dd[ip]) for ip in range(self.nproc) ]

        print(' [*] processing in parallel... ')
        list_dd = pool.starmap(fill_w_desc_and_mulliken, args)
        print(' [+] DONE!\n')
        del dd  # TODO: no borra nada!

        # gather all
        print(' [*] Concatenating... ')
        dd_all = {}
        for cnm in colnames_0 + colnames_1:

            dd_all[cnm] = np.concatenate([ list_dd[ip][cnm] for ip in range(self.nproc) ], axis=0)
           
            print('   [*] removing buffer: ', cnm)
            for ip in range(self.nproc):
                
                del list_dd[ip][cnm]

        print(' [+] DONE!\n')
        del list_dd
        # we don't need this shit anymore
        self.features['dim'].pop('spatialDescriptors_and_Mulliken')

        # no debe quedar ningun NaN!!
        assert all(any(np.isnan(dd_all[knm])) is False for knm in colnames_0 + colnames_1)

        return dd_all



    def _build_and_normalize__desc_and_mullik(self,):

        dd_all = self._scatter_and_gather()

        # calc mean/norm for the descriptors and mulliken charges
        df                   = pd.DataFrame(dd_all)
        scl_mean             = df.mean(axis=0)
        scl_norm_descriptors = np.max((df - scl_mean).drop(['mulliken_charge_0', 'mulliken_charge_1'], axis=1).abs().max())
        scl_norm_mulliken    = np.max((df - scl_mean)[['mulliken_charge_0', 'mulliken_charge_1']].abs().max())
        # total number of spatial descriptors
        n_desc               = len([ _col for _col in df.columns.values if _col.startswith('desc_') ])
        scl_norm             = pd.Series(
            [scl_norm_descriptors]*n_desc + [scl_norm_mulliken, scl_norm_mulliken], 
            index = [ _col for _col in df.columns.values if _col.startswith('desc_') ] + \
                ['mulliken_charge_0', 'mulliken_charge_1']
            )

        # make sure this dataframe has the same number of data-points as the train set
        assert df.shape[0] == self.ds_trn['atom_index_0'].size

        #--- normalized : desccriptors & mulliken
        #df_norm__desc_and_mullik = (df - scl_mean).div(scl_norm, axis = 1)
        df_norm__desc_and_mullik = (df - scl_mean) / scl_norm
        
        return {
            'df_norm'  : df_norm__desc_and_mullik,
            'scl_mean' : scl_mean,
            'scl_norm' : scl_norm, 
            }



    def _normalize__3jxy(self,):

        df = self.features['dim']['interact_3jxy']

        #scl_mean = df['diedral_cos'].mean(axis=0)
        #scl_norm = np.std(df['diedral_cos'] - scl_mean)
        scl_mean = df.mean(axis=0)
        scl_norm = np.std(df - scl_mean, axis=0)

        df_norm = (df - scl_mean) / scl_norm
        
        #assert all(self.features['dim']['interact_3jxy']['idx_3JXY'] == self.features['dim']['interact_3jxy'].index)

        df_norm.drop("idx_3JXY", axis=1, inplace=True)

        return {
            'df_norm'  : df_norm,
            'scl_mean' : scl_mean,
            'scl_norm' : scl_norm,
            'idx'      : self.features['dim']['interact_3jxy']['idx_3JXY'].values,
            }


   
    def _normalize__interactions_all(self):
    
        df = self.features['dim']['interact_all']

        scl_mean = df.mean(axis=0)

        _norm_lj_sum = max(
            np.max(np.abs(df['lj_sum_energy_0'] - scl_mean['lj_sum_energy_0'])),
            np.max(np.abs(df['lj_sum_energy_1'] - scl_mean['lj_sum_energy_1']))
            )
        _norm_lj_mean = max(
            np.max(np.abs(df['lj_mean_energy_0'] - scl_mean['lj_mean_energy_0'])),
            np.max(np.abs(df['lj_mean_energy_1'] - scl_mean['lj_mean_energy_1']))
            )
        scl_norm = pd.Series({
            'rel_dist'            : np.max(np.abs(df['rel_dist'] - scl_mean['rel_dist'])),
            'lj_energy'           : np.max(np.abs(df['lj_energy'] - scl_mean['lj_energy'])),
            'lj_sum_energy_0'     : _norm_lj_sum,
            'lj_sum_energy_1'     : _norm_lj_sum,
            'lj_mean_energy_0'    : _norm_lj_mean,
            'lj_mean_energy_1'    : _norm_lj_mean,
            'coulomb_interaction' : np.max(np.abs(df['coulomb_interaction'] - scl_mean['coulomb_interaction']))
            })

        assert all(scl_norm.index == self.features['dim']['interact_all'].columns), \
            "\n [!] perhaps need to update 'scl_norm' ?\n"

        df_norm__more = (df - scl_mean) / scl_norm

        return {
            'scl_mean' : scl_mean,
            'scl_norm' : scl_norm,
            'df_norm'  : df_norm__more,
            }
   

    
    #--- TODO: random forest TRAINING
    def train__ExtraTrees(self,):
        pass

   

    def train__3jxy_wLightGBM(self, output_model=False):
        """ Train only the 3JXY couplings, with Gradient
        Boosting Decision Trees.
        Model parameters below were tunned interactively running
        in notebooks.
        """

        # report sizes for the record/logs
        train       = self.train_3j
        crossval    = self.cross_val_3j

        ntrain      = self.train_3j["input"].shape[0]
        ncv         = self.cross_val_3j["input"].shape[0]
        ftrain      = 1. * ntrain / (ntrain + ncv)
        print('\n [*] training with train-size = %d (%.2f %%)\n' % (ntrain, ftrain))

        #--- important parameters 
        mparams = {
        "num_leaves"                : 255,              
        "n_estimators"              : int(2**10), #int(2**15),
        "bin_construct_sample_cnt"  : int(0.8 * ntrain),
        "min_data_in_leaf"          : 200,
        "bagging_fraction"          : 0.50,             # fraction of data points to use in each tree
        "bagging_freq"              : 2,                # do baggin every 'bagging_freq' trees
        "feature_fraction"          : 0.7,              # select this fraction of features before training each tree
        "lambda_l1"                 : 0.2,              # L1 regularization (proportional to number of leaves)
        "lambda_l2"                 : 1.2,              # L2 regularization (prop to the sum of scores in all leaves)
        }

        omodel = train_regression_wLightGBM(
            train = self.train_3j,
            crossval = self.cross_val_3j,
            params = mparams,
            output_model = output_model,
            )

        return omodel



    def _load_2jxy_features(self,):
        """ slice data for 2J coupling && split into (train, crossval)
        """

        _idx_2jxy  = list(set(self.idx_train_orig) & set(self.features['adim']['f2jxy__idx']))
        _df        = pd.DataFrame({"dumy": self.idx_train_orig})
        _df["idx"] = _df.index.values
        idx_ord_2j = _df.set_index("dumy", drop=False).loc[_idx_2jxy,"idx"].values
        del _df

        # features
        df_common      = self.features['adim']['common'].drop(idx_ord_2j, axis='index').astype('float32')

        # target
        coupling_const = self.features['adim']['coupling_constant'].drop(idx_ord_2j)

        x_train, x_test, y_train, y_test = train_test_split(
            df_common,
            coupling_const,
            test_size = self.frac_crossval,
            random_state = 0,
            )

        self.train_2j = DB_dict({
            "input"  : x_train,
            "target" : y_train,
            })

        self.crossval_2j = DB_dict({
            "input"  : x_test,
            "target" : y_test,
            })

        print(' ---> 2JXY data:')
        print(' - x_train.shape :', x_train.shape)
        print(' - x_test.shape  :', x_test.shape)



    def _load_1jxy_features(self,):
        """ slice data for 1J coupling && split into (train, crossval)
        """

        _idx_1jxy  = list(set(self.idx_train_orig) & set(self.features['adim']['f1jxy__idx']))
        _df        = pd.DataFrame({"dumy": self.idx_train_orig})
        _df["idx"] = _df.index.values
        idx_ord_1j = _df.set_index("dumy", drop=False).loc[_idx_1jxy,"idx"].values
        del _df

        # features
        df_common      = self.features['adim']['common'].drop(idx_ord_1j, axis='index').astype('float32')

        # target
        coupling_const = self.features['adim']['coupling_constant'].drop(idx_ord_1j)

        x_train, x_test, y_train, y_test = train_test_split(
            df_common,
            coupling_const,
            test_size = self.frac_crossval,
            random_state = 0,
            )

        self.train_1j = DB_dict({
            "input"  : x_train,
            "target" : y_train,
            })

        self.crossval_1j = DB_dict({
            "input"  : x_test,
            "target" : y_test,
            })

        print(' ---> 1JXY data:')
        print(' - x_train.shape :', x_train.shape)
        print(' - x_test.shape  :', x_test.shape)


    def train__2jxy_wLightGBM(self, output_model=False):
        """ train 2JXY
        """

        # report sizes for the record/logs
        train       = self.train_2j
        crossval    = self.crossval_2j

        ntrain      = self.train_2j["input"].shape[0]
        ncv         = self.crossval_2j["input"].shape[0]
        ftrain      = 1. * ntrain / (ntrain + ncv)
        print('\n [*] training with train-size = %d (%.2f %%)\n' % (ntrain, ftrain))

        #--- important parameters 
        mparams = {
        "num_leaves"                : 255,              
        "n_estimators"              : int(2**10), #int(2**15),
        "bin_construct_sample_cnt"  : int(0.8 * ntrain),
        "min_data_in_leaf"          : 200,
        "bagging_fraction"          : 0.50,             # fraction of data points to use in each tree
        "bagging_freq"              : 2,                # do baggin every 'bagging_freq' trees
        "feature_fraction"          : 0.7,              # select this fraction of features before training each tree
        "lambda_l1"                 : 0.2,              # L1 regularization (proportional to number of leaves)
        "lambda_l2"                 : 1.2,              # L2 regularization (prop to the sum of scores in all leaves)
        }

        omodel = train_regression_wLightGBM(
            train = train,
            crossval = crossval,
            params = mparams,
            output_model = output_model,
            )

        return omodel



    def train__1jxy_wLightGBM(self, output_model=False):
        """ train 1JXY
        """

        # report sizes for the record/logs
        train       = self.train_1j
        crossval    = self.crossval_1j

        ntrain      = self.train_1j["input"].shape[0]
        ncv         = self.crossval_1j["input"].shape[0]
        ftrain      = 1. * ntrain / (ntrain + ncv)
        print('\n [*] training with train-size = %d (%.2f %%)\n' % (ntrain, ftrain))

        #--- important parameters 
        mparams = {
        "num_leaves"                : 255,              
        "n_estimators"              : int(2**10), #int(2**15),
        "bin_construct_sample_cnt"  : int(0.8 * ntrain),
        "min_data_in_leaf"          : 200,
        "bagging_fraction"          : 0.50,             # fraction of data points to use in each tree
        "bagging_freq"              : 2,                # do baggin every 'bagging_freq' trees
        "feature_fraction"          : 0.7,              # select this fraction of features before training each tree
        "lambda_l1"                 : 0.2,              # L1 regularization (proportional to number of leaves)
        "lambda_l2"                 : 1.2,              # L2 regularization (prop to the sum of scores in all leaves)
        }

        omodel = train_regression_wLightGBM(
            train = train,
            crossval = crossval,
            params = mparams,
            output_model = output_model,
            )

        return omodel



    def save_crossval(self):

        df_out = pd.DataFrame({
        "id"            : self.idx_crossval,
        "molecule_name" : self.ds_trn["molecule_name"][self.idx_crossval],
        "atom_index_0"  : self.ds_trn["atom_index_0"][self.idx_crossval],
        "atom_index_1"  : self.ds_trn["atom_index_1"][self.idx_crossval],
        "type"          : self.ds_trn["type"][self.idx_crossval],
        })

        fname_out = self.paths_dst['fname_out_crossval']
        print('\n [*] Saving cross-validation dataset to:\n %s\n' % fname_out)
        df_out.set_index("id").to_csv(
            fname_out,
            sep = ','
            )




def train_regression_wLightGBM(train, crossval, params, output_model=False):
    """ Input:
    - train    : dict that contains "input" and "target" keys, where each is
                 a DataFrame.
    - crossval : same but, acts as "test" to evaluate model predictions.
    - params   : model parameters
    """

    #--- define model
    lgbm = lightgbm.LGBMRegressor(
        boosting_type = 'gbdt',
        num_leaves = params['num_leaves'],
        max_depth = -1,
        learning_rate = 0.01,
        n_estimators = params['n_estimators'],
        subsample_for_bin = params['bin_construct_sample_cnt'],
        objective = None,
        class_weight = None,
        min_split_gain = 0.0,
        min_child_weight = 0.001,
        min_child_samples = params['min_data_in_leaf'],
        subsample = params['bagging_fraction'],
        subsample_freq = params['bagging_freq'],
        colsample_bytree = params['feature_fraction'],
        reg_alpha = params['lambda_l1'],
        reg_lambda = params['lambda_l2'],
        random_state = None,
        n_jobs = -1,
        silent = False,
        importance_type = 'split',
        device = 'cpu',
        verbose = 6,
        )

    # Training ...
    t0 = time.time()
    fit_lgbm = lgbm.fit(
        train['input'], 
        train['target'],
        verbose = True,
        )
    print(' [*] training ok. ELP: {:0.02f} s'.format( time.time() - t0 ))

    print('\n [*] predicting...')
    y_pred_trn = lgbm.predict(train['input'])
    y_pred_cv  = lgbm.predict(crossval['input'])

    print('\n [*] calculating prediction errors...\n')
    err_train = y_pred_trn - train["target"].iloc[:,0]
    err_cv    = y_pred_cv - crossval["target"].iloc[:,0]

    log_mae_train = np.mean(np.log10(np.abs(err_train)))
    mae_train     = np.mean(np.abs(err_train))
    mse_train     = np.sqrt(np.mean(np.square(err_train)))

    log_mae_cv = np.mean(np.log10(np.abs(err_cv)))
    mae_cv     = np.mean(np.abs(err_cv))
    mse_cv     = np.sqrt(np.mean(np.square(err_cv)))

    print(" --- train")
    print(" log10(MAE) :: train =", log_mae_train)
    print(" MAE      :: train =", mae_train)
    print(" MSE      :: train =", mse_train)
    print(" --- cross-valid")
    print(" log10(MAE) :: cv    =", log_mae_cv)
    print(" MAE      :: cv    =", mae_cv)
    print(" MSE      :: cv    =", mse_cv)
    print()

    omodel = {
    'trainer' : lgbm,
    'fitter'  : fit_lgbm,
    }

    # performances
    perfs = {
    'model'               : omodel if output_model else None,
    'mae_train'           : mae_train,
    'mae_cv'              : mae_cv,
    'mse_train'           : mse_train,
    'mse_cv'              : mse_cv,
    'feature_importances' : lgbm.feature_importances_,
    }

    return perfs



def test_several_lightgbm(list_param, train, crossval, nproc=4, fname_out=''):

    performances = []

    print('\n [*] Using %d features as input:\n  - ' % len(train['input'].columns), 
        '\n  - '.join(train['input'].columns.values), 
        '\n')

    for ip, param in enumerate(list_param):

        print('\n [*] ------ ROUND #%d/%d\n' % (ip, len(list_param)-1))

        t0 = time.time()

        lgbm = lightgbm.LGBMRegressor(
            boosting_type = 'gbdt',
            num_leaves = param.get('num_leaves', 511),
            max_depth = -1,
            learning_rate = 0.01,
            n_estimators = param.get('n_estimators', 1024),
            subsample_for_bin = 200000,
            objective = None,
            class_weight = None,
            min_split_gain = 0.0,
            min_child_weight = 0.001,
            min_child_samples = 20,
            subsample = 1.0,
            subsample_freq = 0,
            colsample_bytree = 1.0,
            reg_alpha = param.get('reg_alpha' ,0.0),
            reg_lambda = param.get('reg_lambda', 0.0),
            random_state = None,
            n_jobs = nproc,
            silent = False,
            importance_type = 'split',
            device = 'cpu',
            verbose = 6,
            )

        # Entrenando ...
        fit_lgbm = lgbm.fit(
            train['input'], 
            train['target'],
            verbose = True,
            )

        print(' [*] training ok. ELP: {:0.02f} s'.format( time.time() - t0 ))

        print('\n [*] predicting...')
        y_pred_trn = lgbm.predict(train['input'])
        y_pred_cv  = lgbm.predict(crossval['input'])

        print('\n [*] calculating prediction errors...\n')
        err_train = y_pred_trn - train["target"].iloc[:,0]
        err_cv    = y_pred_cv - crossval["target"].iloc[:,0]

        log_mae_train = np.mean(np.log10(np.abs(err_train)))
        mae_train     = np.mean(np.abs(err_train))
        mse_train     = np.sqrt(np.mean(np.square(err_train)))

        log_mae_cv = np.mean(np.log10(np.abs(err_cv)))
        mae_cv     = np.mean(np.abs(err_cv))
        mse_cv     = np.sqrt(np.mean(np.square(err_cv)))

        print(" log(MAE) :: train =", log_mae_train)
        print(" MAE      :: train =", mae_train)
        print(" MSE      :: train =", mse_train)
        print(" log(MAE) :: cv    =", log_mae_cv)
        print(" MAE      :: cv    =", mae_cv)
        print(" MSE      :: cv    =", mse_cv)

        performances += [ {
        'params'              : param,
        'mae_train'           : mae_train,
        'mae_cv'              : mae_cv,
        'mse_train'           : mse_train,
        'mse_cv'              : mse_cv,
        'feature_importances' : lgbm.feature_importances_,
        } ]

    if fname_out:
        pickle.dump(performances, open(fname_out, 'bw'))

    return performances




def fill_w_desc_and_mulliken(mol_names, IDXs_atom_0, IDXs_atom_1, df_inp, dd):

    assert mol_names.size == IDXs_atom_0.size == IDXs_atom_1.size
    n_inp = IDXs_atom_0.size

    for ia, (_name_mol, _iatom0, _iatom1) in enumerate(zip(mol_names, IDXs_atom_0, IDXs_atom_1)):

        if ia % 5000 == 0:
            print(f'   > [proc::{mp.current_process().name}] {ia+1}/{n_inp}')

        # grab the spatial descriptors and mulliken charges (for atom o and 1)
        dscs_and_mull_0 = df_inp.loc[(_name_mol, _iatom0), :].iloc[0]
        dscs_and_mull_1 = df_inp.loc[(_name_mol, _iatom1), :].iloc[0]

        knm_list = [ _cnm + '_0' for _cnm in dscs_and_mull_0.index.values.tolist() ] + \
            [ _cnm + '_1' for _cnm in dscs_and_mull_1.index.values.tolist() ]

        values   = dscs_and_mull_0.values.tolist() + dscs_and_mull_1.values.tolist()

        for knm, value in zip(knm_list, values):

            dd[knm][ia] = value

        # i need to know that this obvious thing happens always
        #assert all([ _cnmv1 == _cnmv2 for (_cnmv1, _cnmv2) in zip(colnames_0 + colnames_1, knm_list) ])

    return dd



if __name__ == '__main__':

    paths_src = {'dir_src':'/media/sergio/EVO970/Kaggle/molecules/kaggle_molecules/data',
                'fname_inp_spatial_desc':'/media/sergio/EVO970/Kaggle/molecules/kaggle_molecules/data/struct_descriptors_ii.csv',
                'fname_inp_more':'/media/sergio/EVO970/Kaggle/molecules/kaggle_molecules/data/interaction_desc.h5'}

    
    frac_crossval=0.75
    nproc=4
    frac_inp=0.1
    
    trainer = jcoupling_trainer(paths_src, frac_crossval, nproc, frac_inp)
    





#EOF
