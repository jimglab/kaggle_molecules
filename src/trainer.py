#!/usr/bin/env ipython
import sys, os, argparse
from os.path import isfile, isdir, dirname
from glob import glob
import joblib

# custom libs
import funcs as ff
import sfuncs as sff
import funcs_train as fft

# graphics 
import numpy as np
import matplotlib.pyplot as plt


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-frac-inp',
type=float,
default=0.1,
help='N of the sub-sample of all the molecules.',
)
parser.add_argument(
'-frac-crossval',
type=float,
default=0.0,
help="""fraction of the whole train set to use as cross-validation. 
If !=0 we'll generate an ASCII output in the kaggle-test.csv format 
for later prediction. If ==0, we'll train the whole train set && won't
generate any ASCII output.
""",
)
parser.add_argument(
'-np', '--nproc',
type=int,
default=0,
help='number of processors.',
)
parser.add_argument(
'-dir-src',
default='./data',
help='directory where kaggle data is stored.',
)
parser.add_argument(
'-target',
default='coupling',
help='for which target we will train.',
)
parser.add_argument(
'-dir-dst',
default='./out',
help='filename prefix for output model.',
)
parser.add_argument(
'-fname-out-crossval',
default='crossval.txt',
help="""ASCII output in the same format as the test.csv from Kaggle. 
This is so we can use it as input for prediction with the same script
that we use for test.csv""",
)
pa = parser.parse_args()

#sys.exit(0)
#--- input paths
#dirs_src = {
#    'dir_kagle'                 : pa.dir_src,
#    'fname_spatial_descriptors' : pa.fname_descriptors,
#    'fname_more'                : pa.fname_more,
#}

assert pa.target in ('mulliken', 'coupling')


paths_src = {}
paths_dst = {}
# check that we have necessary stuff for this target
if pa.target == "coupling":

    #--- input paths
    paths_src['dir_src'] = pa.dir_src
    assert isdir(paths_src['dir_src'])

    #paths_src['fname_inp_mulliken'] = pa.dir_src + '/postproc/model__mulliken.pkl'
    #assert isfile(paths_src['fname_inp_mulliken']), \
    #    '\n [-] we need the Mulliken training (.pkl) !!\n'

    # spatial descriptors (radial and angular)
    paths_src['fname_inp_spatial_desc'] = pa.dir_src + '/postproc/spatial_descriptors.csv'
    paths_src['fname_inp_more']         = pa.dir_src + '/postproc/interaction_features.h5'
    assert isfile(paths_src['fname_inp_spatial_desc']) and isfile(paths_src['fname_inp_more']), \
        "\n [-] either the spatial descriptors and/or the interactions input file is missing!\n"

    #--- output paths
    fout_model_prefix = pa.dir_dst + '/model_coupling_constant'
    # TODO: usar un flag CLI para forzar pisado si ya existe el modelo en disco
    assert not any(isfile(_fnm) for _fnm in glob(fout_model_prefix+'*')), \
        '\n [-] you have a [coupling constant] model in the output directory! (%s)\n' % (fout_model_prefix+'*')

    paths_dst['fname_out_crossval'] = pa.fname_out_crossval

    paths_dst['dir_dst'] = pa.dir_dst
    assert isdir(paths_dst['dir_dst'])

elif pa.target == "mulliken":

    paths_src['fname_out_model'] = pa.dir_src + '/model__mulliken.pkl'
    # TODO: usar un flag CLI para forzar pisado si ya existe el modelo en disco
    assert isfile(paths_src['fname_out_model']), \
        f'\n [-] the model already exists:\n {fname_out_model}\n'

else:

    raise SystemExit('\n [-] unknown target (%s)' % paths_src['fname_out_model'])



#
# collect features
#
t3j = fft.jcoupling_trainer(
    paths_src,
    paths_dst = paths_dst,
    frac_crossval = pa.frac_crossval,
    nproc = pa.nproc,
    frac_inp = pa.frac_inp,
    )


t3j._load__spatial_descriptors_and_mulliken()


#--- read features from the hdf5 (Couloumb, Lennard-Jones, diedral angles)
t3j._load__interaction_energies()


# - normalize L-J energies
# - normalize Coulomb
t3j._collect_features()


#--- TODO: separate into train/cross-validation sets
t3j._prepare__train_and_cv()

if pa.frac_crossval > 0:

    t3j.save_crossval()


t3j._load_3jxy_features()

t3j._load_2jxy_features()

t3j._load_1jxy_features()



#--- train && save model :: 3J
model_3j = t3j.train__3jxy_wLightGBM(output_model = True)
fname_out_model3j = fout_model_prefix + '_3j.txt'
model_3j["model"]["trainer"].booster_.save_model(fname_out_model3j)


#--- train && save model :: 2J
model_2j = t3j.train__2jxy_wLightGBM(output_model = True)
fname_out_model2j = fout_model_prefix + '_2j.txt'
model_2j["model"]["trainer"].booster_.save_model(fname_out_model2j)


#--- train && save model :: 1J
model_1j = t3j.train__1jxy_wLightGBM(output_model = True)
fname_out_model1j = fout_model_prefix + '_1j.txt'
model_1j["model"]["trainer"].booster_.save_model(fname_out_model1j)




#EOF
