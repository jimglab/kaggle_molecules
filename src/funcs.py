#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import random
from scipy.linalg import norm
from scipy.spatial.distance import cdist

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LogNorm, Normalize
from pylab import figure, close
import seaborn as sns
import os, sys

# Sergio's functions
import sfuncs as sff



class molecule_mgr:
    """
    Meotodos para:
    * plotar 3D a alguna molecula en particulas
    * lista de nombre unicos de molecula
    * etc
    """

    def __init__(self, fname_inp='', dir_dst='', **kws):

        if fname_inp: 
            assert os.path.isfile(fname_inp)

        if dir_dst:
            assert os.path.isdir(dir_dst)

        # generate list of unique molecule names
        self.m_structs = sff.load_ds(fname_inp)

    
    def struct_3d(self, molecule_name):
        pass


#--- BEGIN: symmetry functions
# Source:
# https://www.kaggle.com/borisdee/predicting-mulliken-charges-with-acsf-descriptors/notebook

def fc(Rij, Rc):
    """ Compact support radial symmetry function
    """

    if type(Rij) is np.ndarray:

        _y = [ 0.5 * (np.cos(np.pi * r / Rc) + 1) if r<=Rc else 0. for r in Rij ]
        return np.array(_y)

    else:

        _ans = 0.5 * (np.cos(np.pi * Rij / Rc) + 1) if Rij <= Rc else 0.0
        return _ans



def symm_G1(Rij, Rc):
    """ G1 symmetry function
    """
    return np.sum(fc(Rij, Rc))
    


def symm_G2(Rij, eta, Rs, Rc, terms=False):
    """ G2 symmetry function
    """
    _terms = np.exp(-eta * np.square(Rij - Rs)) * fc(Rij, Rc)
    sum_terms = np.sum(_terms)
    
    return (sum_terms, _terms) if terms else sum_terms



def symm_G2_radial(R, params):
    """ radial symm func
    R: coords of atoms, with shape (N, 3), where N is the number
       of atoms in the molecule.
    """

    n_atoms   = R.shape[0] # number of atoms in this molecule

    g2 = np.nan * np.ones(n_atoms)
    for i in range(n_atoms):

        Rij = np.array([ norm(R[j] - R[i]) for j in range(n_atoms) if j!=i ])
        #print(" > sum-sqr = ", np.square(Rij).sum())

        g2[i] = symm_G2(Rij, params["eta"], params["Rs"], params["Rc"])

    return g2



def symm_G2_angular(R, params):
    """ angular symm function
    R: coords of atoms, with shape (N, 3), where N is the number
       of atoms in the molecule.
    """

    Lambda = params["lambda"] # +1, -1
    eta    = params["eta"]
    psi    = params["psi"]
    Rc     = params["Rc"]

    # NOTE: the index of this array inherits (checked!) the 
    # "atom_index" in the "structures.csv" file.
    summation = np.zeros(R.shape[0])

    for i, ri in enumerate(R):
        
        for j, rj in enumerate(R):

            if j==i:
                continue

            rij = rj - ri; rij_mod = norm(rij)

            # TODO: esto se puede en 1 sola linea??
            Rik = np.array([ rk - ri for k, rk in enumerate(R) if k!=i ])
            Rjk = np.array([ rk - rj for k, rk in enumerate(R) if k!=i ])

            Rik_mod = norm(Rik, axis=1)
            Rjk_mod = norm(Rjk, axis=1)

            _dot   = np.array([ np.dot(rij, rik) for rik in Rik ])
            cos_th = _dot / (rij_mod * Rik_mod)

            _term  = np.power(1.0 + Lambda*cos_th, psi)
            _term *= np.exp(-eta * (rij_mod**2 + Rik_mod**2 + Rjk_mod**2))
            _term *= fc(rij_mod, Rc) * fc(Rik_mod, Rc) * fc(Rjk_mod, Rc)

            summation[i] += _term.sum() #np.sum(_term)

    summation[:] *= np.power(2.0, 1. - psi)

    return summation

    
#--- END: symmetry functions



def build_df_wDescriptors(struct_dict, molecule_list, desc_params):

    params_g2ang, params_g2rad = desc_params["g2ang"], desc_params["g2rad"]
    nmols = len(molecule_list)

    df = pd.DataFrame()
    for imol, mol_name in enumerate(molecule_list):

        if imol % 50 == 0:
            print(' > imol: %d/%d' % (imol, nmols-1))

        R = struct_dict[mol_name][0]
        n_atoms = R.shape[0] # number of atoms in this molecule

        #--- 3-point symmetry function G_(r,phi)^(4)
        descriptors_g2ang = {}
        for _nm in params_g2ang.keys():
            descriptors_g2ang[_nm] = symm_G2_angular(R, params_g2ang[_nm])

        #--- 2-point symmetry function G_r^(2)
        descriptors_g2rad = {}
        for _nm in params_g2rad.keys():
            descriptors_g2rad[_nm] = symm_G2_radial(R, params_g2rad[_nm])

        atom_names = struct_dict[mol_name][1]

        _df = pd.DataFrame(dict(
            [ ("molecule_name", [ mol_name ]*n_atoms), ] + \
            [ ("atom_index", np.arange(n_atoms)) ] + \
            [ ("atom", atom_names.tolist()) ] + \
            [ ("desc_g2ang_%02d" % _nm, descriptors_g2ang[_nm]) for _nm in descriptors_g2ang.keys() ] + \
            [ ("desc_g2rad_%02d" % _nm, descriptors_g2rad[_nm]) for _nm in descriptors_g2rad.keys() ]
            ))

        df = pd.concat(
            [df, _df], 
            sort=False
            )

    return df



def main1():

    import random
    mm = molecule_mgr("../data/structures.csv")

    ds_train    = sff.load_ds('../data/train.csv')

    struct_dict = sff.make_struct_dict(mm.m_structs)

    for mname in random.sample(struct_dict.keys(), 15):

        print(" > " + mname)

        fig, ax = sff.plot_molecule(struct_dict, molecule_name=mname, ds=ds_train)

        plt.show()



def histogram_of_distances(n_sample=0, fname_struct="../data/structures.csv", verb=True):
    """
    n_sample: max number of distances we'll calculate
    """

    mm = molecule_mgr(fname_struct)

    struct_dict = sff.make_struct_dict(mm.m_structs)
    nmols = len(struct_dict.keys())

    i_sample = 0
    dist = []
    for imol, mol_name in enumerate(struct_dict.keys()):

        if verb and imol % 50 == 0:
            print(' > imol: %d/%d' % (imol, nmols-1))

        R = struct_dict[mol_name][0]
        n_atoms = R.shape[0] # number of atoms in this molecule

        idx_pairs = [ (i, j) for i in range(n_atoms) for j in range(i+1, n_atoms) ]

        for (i, j) in idx_pairs:

            dist += [ norm(R[j] - R[i]) ]

            i_sample += 1
            if n_sample and i_sample >= n_sample: break

        if n_sample and i_sample >= n_sample: break

    return dist



def equi_bounds(dini, dend, n):
    """
    bounds in 'n' equipartitioned segments inside a numerical
    range between 'dini' and 'dend'; starting in 'start'
    """
    interv = equi_intervals(dini, dend, n).cumsum()
    bd = []
    bd += [ dini ]
    for it in interv:
        bd += [ it ] 

    bd = np.array(bd)
    return bd



def equi_intervals(dini, dend, n): 
    """ 
    returns most equi-partitioned tuple in the numerical
    range between 'dini' and 'dend'
    """
    #days = (dend - dini).days
    days = dend - dini + 1
    days_part = np.zeros(n, dtype=np.int)
    resid = np.mod(days, n)
    for i in range(n-resid):
        days_part[i] = days/n

    # last positions where I put residuals
    last = np.arange(start=-1,stop=-resid-1,step=-1)
    for i in last:
        days_part[i] = days/n+1

    assert np.sum(days_part)==days, \
        " --> somethng went wrong!  :/ "

    return days_part



def biplot(score, coeff, labels=None, color='b', figax=None):

    #from pylab import figure

    xs     = score[:,0]
    ys     = score[:,1]
    n      = coeff.shape[0]
    scalex = 1.0/(xs.max() - xs.min())
    scaley = 1.0/(ys.max() - ys.min())
    
    if figax is None:
        fig = figure(1, figsize=(9,7))
        ax  = fig.add_subplot(111)

    else:
        fig, ax = figax
    
    ax.scatter(xs * scalex, ys * scaley, c = color)
    
    for i in range(n):
        ax.arrow(0, 0, coeff[i,0], coeff[i,1], color = 'r', alpha = 0.45, head_width=0.04, head_length=0.1)
        
        if labels is None:
            ax.text(coeff[i,0]* 1.15, coeff[i,1] * 1.15, "Var"+str(i+1), color='g', ha='center', va='center')
            
        else:
            ax.text(coeff[i,0]* 1.15, coeff[i,1] * 1.15, labels[i], color='g', ha='center', va='center')
            
    ax.set_xlim(-1,1)
    ax.set_ylim(-1,1)
    ax.set_xlabel("PC{}".format(1))
    ax.set_ylabel("PC{}".format(2))
    ax.grid(ls='--')
    
    return fig, ax


def make_hist2d(x, y, bins=(50, 50), range=(None, None), **kws):

    h = np.histogram2d(
        x, y, 
        bins = bins, 
        range = range,
        normed = False,
        )

    h2d = h[0].T
    xbins = 0.5 * (h[1][1:] + h[1][:-1])
    ybins = 0.5 * (h[2][1:] + h[2][:-1])

    fig = figure(1, figsize=kws.get('figsize', (6, 4)))
    ax  = fig.add_subplot(111)

    fig, ax = contour_2d(
        xbins, ybins, h2d,
        figax = (fig, ax),
        hscale = 'log',
        cb_label = 'puntos por bin cuadrado',
        cb_fontsize = kws.get('cb_fontsize', 16),
        vlim = (h2d.min()+1, h2d.max()),
        kind = 'imshow',
        cmap = kws.get('cmap', 'viridis'),
        )

    ax.set_xlim(range[0])
    ax.set_ylim(range[1])

    return fig, ax



def contour_2d(x, y, mat, figax, hscale='log', kind='', **kargs):

    fig, ax = figax

    cb_label = kargs.get('cb_label', 'points per bin square')
    cb_fontsize = kargs.get('cb_fontsize', 15)
    cbmin, cbmax = kargs.get('vlim', (1, 1e3))

    opt = {
    'linewidth': 0,
    'cmap': getattr(cm, kargs.get('cmap', 'gray_r')), #cm.gray_r,    # gray-scale
    'vmin': cbmin, #kargs.get('cbmin',1),
    'vmax': cbmax, #kargs.get('cbmax',1000),
    'alpha': kargs.get('alpha',0.9),
    }

    if hscale=='log':
        opt.update({'norm': LogNorm(),})

    #--- 2d contour
    if kind == 'contourf':
        surf = ax.contourf(x, y, mat, facecolors=cm.jet(mat), **opt)

    elif kind == 'imshow':
       
        opt.pop('linewidth')
        xlim = np.min(x), np.max(x)
        ylim = np.min(y), np.max(y)
        surf = ax.imshow(
            mat,
            interpolation = 'none',
            extent = (xlim[0], xlim[1], ylim[1], ylim[0]),
            **opt
            )

    sm = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    sm.set_array(mat)

    #--- colorbar
    axcb = fig.colorbar(sm)
    axcb.set_label(cb_label, fontsize=cb_fontsize)
    sm.set_clim(vmin=cbmin, vmax=cbmax)
    axcb.ax.tick_params(labelsize=cb_fontsize)

    return fig, ax



#+++++++++++++++++++++++++++++++++++++++
#
# INTERACTION ENERGIES
#

def rel_distances(ds, struct_dict, idx_v=(-1,),):
    """
    Calculate relative distances between coupled atoms.

    idx_v : tuple of indexes of `ds["atom_index_0"][:]` (or iterator), for the couplings of interest
    """

    if len(idx_v)==1 and idx_v[0] == -1:
        idx_v = range(0, ds["atom_index_0"].shape[0])

    # relative distance modulus of all couplings of interest
    dist = np.empty(len(idx_v), dtype=np.float32)

    for ic in idx_v:

        molecule_name = ds['molecule_name'][ic]

        idx_0 = ds['atom_index_0'][ic]
        idx_1 = ds['atom_index_1'][ic]

        # pos_v: Array posicion para todos los atomos
        # atm_v: Array de tipo de ´atomo
        pos_v = struct_dict[molecule_name][0]

        # distance vector
        dist[ic] = norm(pos_v[idx_1] - pos_v[idx_0])

    return dist



def LJ_interaction_energy(sig_o_r):

    return (sig_o_r**12 - sig_o_r**6)



def lj_energies_between_coupled(ds, struct_dict, sigma=5., rel_dist=None, idx_v=(-1)):
    """ Lennard-Jones interaction energy

    Calculated for between pairs of coupled atomss.
    
    A reasonable cutoff range is rc=12.5 Angstrom; and rc=2.5*sigma, so
    the corresponding sigma=5.
    """

    if len(idx_v)==1 and idx_v[0] == -1:
        idx_v = range(0, ds["atom_index_0"].shape[0])

    lj_energy = np.empty(len(idx_v), dtype=np.float32)

    if rel_dist is None:
        r = rel_distances(ds, struct_dict, idx_v)
    else:
        r = rel_dist

    sig_o_r = sigma / r

    #lj_energy = sig_o_r**12 - sig_o_r**6
    lj_o      = LJ_interaction_energy(1/2.5) # potential offset
    lj_energy = np.maximum(LJ_interaction_energy(sig_o_r) - lj_o, 0.0)

    return lj_energy



def coulomb_between_coupled(ds, struct_dict, rel_dist=None, idx_v=(-1)):
    """ Coulomb interaction energy

    Calculated for between pairs of coupled atomss.
    """

    if len(idx_v)==1 and idx_v[0] == -1:
        idx_v = range(0, ds["atom_index_0"].shape[0])

    nc = len(idx_v) # total number of couplings

    if rel_dist is None:
        r = rel_distances(ds, struct_dict, idx_v)
    else:
        r = rel_dist

    # Ok, these atomic numbers are extracted from:
    # https://en.wikipedia.org/wiki/List_of_chemical_elements
    zdict = {
    'H': 1,
    'C': 6,
    'N': 7,
    'O': 8,
    'F': 9,
    }

    # determine the electrical charges
    # Z[i,0] is the atomic number of the atom-0 in the i-coupling.
    # Z[i,1] is the atomic number of the atom-1 in the i-coupling.
    Z  = np.empty((nc,2), dtype=np.int32)

    for ic in idx_v:

        molecule_name = ds['molecule_name'][ic]
       
        idx_0 = ds['atom_index_0'][ic]
        idx_1 = ds['atom_index_1'][ic]

        # pos_v: Array posicion para todos los atomos
        # atm_v: Array de tipo de atomo
        atm_v = struct_dict[molecule_name][1]
        z = list(map(lambda atype: zdict[atype], atm_v))

        Z[ic,:] = [z[idx_0], z[idx_1]]

    #---
    c_energy = Z[:,0] * Z[:,1] / r

    return c_energy



def rel_distances_respect_to_all(ds, struct_dict, idx_v=(-1,),):
    """
    Calculate relative distances of the [coupled] atoms respect
    to all the other atoms in the molecule.

    idx_v : tuple of indexes of `ds["atom_index_0"][:]` (or iterator), for the couplings of interest
    """

    if len(idx_v)==1 and idx_v[0] == -1:
        idx_v = range(0, ds["atom_index_0"].shape[0])

    # relative distance modulus of all couplings of interest
    dist_0, dist_1 = [], []

    # iterate over each coupling data point
    for ic in idx_v:

        molecule_name = ds['molecule_name'][ic]

        idx_0 = ds['atom_index_0'][ic]
        idx_1 = ds['atom_index_1'][ic]

        # pos_v: Array posicion para todos los atomos
        # atm_v: Array de tipo de atomo
        pos_v = struct_dict[molecule_name][0]

        # distance vector
        idx_rest_0 = [ i for i in range(pos_v.shape[0]) if i!=idx_0 ]
        idx_rest_1 = [ i for i in range(pos_v.shape[0]) if i!=idx_1 ]
        dist_0    += [ cdist(pos_v[idx_0:idx_0+1], pos_v[idx_rest_0]) ] # distances respect to atom-0
        dist_1    += [ cdist(pos_v[idx_1:idx_1+1], pos_v[idx_rest_1]) ] # distances respect to atom-1

    return dist_0, dist_1



def lj_energies_between_all(ds, struct_dict, sigma=5., idx_v=(-1,)):
    """
    Mean and total interaction energies of each coupled atom against
    every other atom in the molecule.

    Output: mean and total, for each atom in the coupled pair.
    """

    if len(idx_v)==1 and idx_v[0] == -1:
        idx_v = range(0, ds["atom_index_0"].shape[0])

    # interaction energies (for atom-0 and atom-1)
    nc = len(idx_v) # number of couplings
    lj_mean_energy_0, lj_sum_energy_0 = np.empty((2, nc), dtype=np.float32)
    lj_mean_energy_1, lj_sum_energy_1 = np.empty((2, nc), dtype=np.float32)

    # get all the inter-distances (of each coupled atom against all the
    # rest in the molecule). So we have a list of inter-distnaces for atom-0 
    # and atom-1.
    rs_0, rs_1 = rel_distances_respect_to_all(ds, struct_dict, idx_v=idx_v)
    assert len(rs_0) == len(rs_1) == len(idx_v)

    lj_o = LJ_interaction_energy(1/2.5)
    for ic in idx_v:

        n_neigh = rs_0[ic].size # total number of neighbours

        # interaction energies for atom-0
        _energies_against_all = np.maximum(LJ_interaction_energy(sigma / rs_0[ic]) - lj_o, 0.0)
        lj_sum_energy_0[ic]   = _energies_against_all.sum()
        lj_mean_energy_0[ic]  = lj_sum_energy_0[ic] / n_neigh

        # interaction energies for atom-1
        _energies_against_all = np.maximum(LJ_interaction_energy(sigma / rs_1[ic]) - lj_o, 0.0)
        lj_sum_energy_1[ic]   = _energies_against_all.sum()
        lj_mean_energy_1[ic]  = lj_sum_energy_1[ic] / n_neigh

    # NOTE: it seems that `x*(x>0)` is faster than `np.maximum(x, 0)`; see:
    # https://stackoverflow.com/questions/32109319/how-to-implement-the-relu-function-in-numpy#40013151

    _dict = {
    'sum_energy_0'  : lj_sum_energy_0,
    'sum_energy_1'  : lj_sum_energy_1,
    'mean_energy_0' : lj_mean_energy_0,
    'mean_energy_1' : lj_mean_energy_1,
    }
    return _dict
                




if __name__ == "__main__":

    #sys.path.insert(0, '<PATH>')
    mm = molecule_mgr("../data/structures.csv")

    ds_train    = sff.load_ds('../data/train.csv')

    struct_dict = sff.make_struct_dict(mm.m_structs)

    params_g2ang = {
    0: {
        'lambda' : 1.,
        'eta'    : (1/1)**2,
        'psi'    : 1,
        'Rc'     : 10,
        },

    1: {
        'lambda' : -1.,
        'eta'    : (1/1)**2,
        'psi'    : 1,
        'Rc'     : 10,
        },

    2: {
        'lambda' : 1.,
        'eta'    : (1/0.5)**2,
        'psi'    : 1,
        'Rc'     : 10,
        },

    3: {
        'lambda' : -1.,
        'eta'    : (1/0.5)**2,
        'psi'    : 1,
        'Rc'     : 10,
        },

    4: {
        'lambda' : 1.,
        'eta'    : (1/2.)**2,
        'psi'    : 1,
        'Rc'     : 10,
        },

    5: {
        'lambda' : -1.,
        'eta'    : (1/2.)**2,
        'psi'    : 1,
        'Rc'     : 10,
        },

    }

    # NOTE: as "rule of thumb", I'm using twice the observed max value 
    # of distance between atoms, for Rc.
    params_g2rad = {
    0: {
        'eta' : (1/0.5)**2,
        'Rc'  : 10.0,
        'Rs'  : 1.4,
        },

    1: {
        'eta' : (1/0.5)**2,
        'Rc'  : 10.0,
        'Rs'  : 2.5,
        },

    2: {
        'eta' : (1/0.7)**2,
        'Rc'  : 10.0,
        'Rs'  : 3.7,
        },

    3: {
        'eta' : (1/0.7)**2,
        'Rc'  : 10.0,
        'Rs'  : 5.0,
        },
    }

    df = pd.DataFrame()

    molecule_list = random.sample(struct_dict.keys(), 301) #301)
    nmols = len(molecule_list)

    for imol, mol_name in enumerate(molecule_list):

        if imol % 50 == 0:
            print(' > imol: %d/%d' % (imol, nmols-1))

        R = struct_dict[mol_name][0]
        n_atoms = R.shape[0] # number of atoms in this molecule

        #--- 3-point symmetry function G_(r,phi)^(4)
        descriptors_g2ang = {}
        for _nm in params_g2ang.keys():
            descriptors_g2ang[_nm] = symm_G2_angular(R, params_g2ang[_nm])

        #--- 2-point symmetry function G_r^(2)
        descriptors_g2rad = {}
        for _nm in params_g2rad.keys():
            descriptors_g2rad[_nm] = symm_G2_radial(R, params_g2rad[_nm])

        _df = pd.DataFrame(dict(
            [ ("molecule_name", [ mol_name ]*n_atoms), ("atom_index", np.arange(n_atoms)) ] + \
            [ ("desc_g2ang_%02d" % _nm, descriptors_g2ang[_nm]) for _nm in descriptors_g2ang.keys() ] + \
            [ ("desc_g2rad_%02d" % _nm, descriptors_g2rad[_nm]) for _nm in descriptors_g2rad.keys() ]
            ))

        df = pd.concat(
            [df, _df], 
            sort=False
            )




#EOF
