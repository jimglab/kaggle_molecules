#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import random
from scipy.linalg import norm
import matplotlib.pyplot as plt
from pylab import figure, close
import seaborn as sns
import os, sys, argparse
import multiprocessing as mp
import missingno

# machine learning
import sklearn.cluster
import sklearn
import sklearn.preprocessing
import sklearn.decomposition
from sklearn.model_selection import train_test_split

# deep learning
from keras.models import Sequential 
from keras.layers.core import Dense, Activation
from keras.utils import np_utils

# let's try w a "random forest"
# Inspired from:
# https://www.kaggle.com/borisdee/predicting-mulliken-charges-with-acsf-descriptors/notebook
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.metrics import mean_absolute_error


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
'-sample',
type=int,
default=0,
help='N of the sub-sample of all the molecules',
)
parser.add_argument(
'-fo', '--fname_out',
type=str,
default='',
help='output .csv',
)
parser.add_argument(
'-ds', '--dir-src',
type=str,
default='',
help='directory of the input dataset',
)
parser.add_argument(
'-w', '--workdir',
type=str,
default='/work/kaggle/molecules/src',
help='absolute dir of the library',
)
parser.add_argument(
'-np', '--nproc',
type=int,
default=0,
help='number of processors',
)
pa = parser.parse_args()

#--- custom libs
if pa.workdir:
    sys.path.insert(0, pa.workdir)

import sfuncs as sff
import funcs as ff


# > Descriptores de la distribucion espacial de los átomos
# entre sí (en terminos de distancia y distribuciones angulares)
df = pd.read_csv(f"{pa.dir_src}/postproc/descriptors_sample.csv", sep=';')
df.drop("Unnamed: 0", axis=1, inplace=True)

# > Structure file
df_struct = pd.read_csv(f"{pa.dir_src}/structures.csv", sep=',')

# Coupling constants
#df_coupling = pd.read_csv("../data/train.csv", sep=',')

# > Mulliken charges
df_mulliken = pd.read_csv(f"{pa.dir_src}/mulliken_charges.csv", sep=',')

assert "molecule_name" in df.columns, "molecule_name" in df_mulliken.columns


# >> merge :
# - dscriptors
# - atom names
# - mulliken charges (in the train set)
df_merge = pd.merge(df, df_mulliken, 
    how="left", 
    on=["molecule_name", "atom_index"])
df_merge = pd.merge(df_merge, df_struct[["molecule_name", "atom_index", "atom"]], 
    how='left', 
    on=["molecule_name", "atom_index"])


# remove rows with mulliken==NaN (due to merge procedure)
df_merge.dropna(axis=0, how='any', subset=["mulliken_charge",], inplace=True)
# reset the goddam indexes
df_merge.reset_index(drop=True, inplace=True)

# -- split train/test
X_train, X_test, Y_train, Y_test = train_test_split(
    df_merge.drop(["molecule_name", "atom_index", "mulliken_charge", "atom"], axis=1),
    df_merge[["mulliken_charge",]],
    train_size=0.75
    )


#--- remove mean && normalize inputs
scl_mean = X_train.mean(axis=0)
X_train_cent = X_train - scl_mean
scl_norm = np.max(X_train_cent.abs().max())
# NOTE: this is the actual input to the training
X_train_norm = pd.DataFrame(X_train_cent / scl_norm, dtype=np.float32)


# seed for keras
np.random.seed(2019)

#--- Building the FCN Model
model = Sequential()     
model.add(Dense(10, input_dim=X_train.shape[1], activation='relu'))
# output = relu (dot (W, input) + bias)
model.add(Dense(4, activation='relu'))
model.add(Dense(1))

# Compilation
model.compile(
    loss = 'mean_absolute_error', 
    optimizer = 'adam', #'sgd'
    )
model.summary()

# Fitting on Data
hist = model.fit(X_train_norm.values, Y_train.values,
    batch_size = int(0.01 * X_train.shape[0]),
    epochs = 128, # 512
    validation_split = 0.2,
    verbose = 2)


#-- atom colors (for train set)
atom_types  = df_merge.loc[X_test.index.values,"atom"].unique()
atom_colors = ["orange", "r", "m", "y", "b"]
colors = dict([ (nm,catom) for (nm,catom) in zip(atom_types, atom_colors) ])
atom_colors_train = df_merge.loc[X_test.index.values,"atom"].apply(lambda nm: colors[nm]).values



score = model.evaluate((X_test - scl_mean)/scl_norm, Y_test, verbose=1)
print('\nTest accuracy:', score)

y_pred = model.predict((X_test - scl_mean).values/scl_norm)
y_pred

print(" --> plotting...")
fig = figure(figsize=(16,10))
ax  = fig.add_subplot(111)

for atom_type, atom_color in zip(atom_types, atom_colors):

    cc = atom_colors_train==atom_color
    ax.scatter(Y_test[cc], y_pred[cc], s=1, c=atom_color, label=atom_type)

ax.plot(Y_test, Y_test, '--k', alpha=0.5)
ax.grid(ls='--', alpha=0.3)
ax.set_title("Mulliken charges", fontsize=18)
ax.set_xlabel("test", fontsize=18)
ax.set_ylabel("pred", fontsize=18)
ax.tick_params(labelsize=16)

ax.legend(loc="best")

#plt.show()

#EOF
