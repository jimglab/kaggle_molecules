

Features to consider:

[ ] masa "efectiva" de c/ atomo.


[ ] hallar grupos de simetria de la molecula

    * simetria de rotacion, reflexion


[ ] "co-planaridad", xq muchas moleculas suelen estar concentradas a lo largo de un 
  plano en particular. Si bien no estrictamente a veces.


[ ] para un mismo par de tipo de enlace -- e.g. ("H", "C") -- hay muchos casos q estan
  resultando en un "mismo" valor de "g". Es decir, para diversas "condiciones de vecinos",
  hay muchos configuraciones q estan dando como resultado un mismo acoplamiento.

  Para el caso ("H", "C") hay basicamnte 2 grupos de "g"; hay q identificar que factor(es)
  estan particionando esos dos clusters.


[ ] buscar `g` para molecula "conocidas" (con o son grupos de simetria, pero lo mas probable
  es q exista mas analisis/investigacion para las q tengan alguna simetria).
  Seran ùtiles para tener una aproximacion de las moleculas en nuestro train set?


[ ] distrib de los valores de c/ termino/contribucion al `g` para / tipo de acoplamiento.

    * buscar biblio del marco teorico sobre las contribuciones


[+] transformar coordenadas espaciales a "Descriptores/Fingreprints" usando las funciones 
    de simetria.

    * buscar clusters con estos features para ver si podemos encontrar grupos de 
    estructuras similares (quiero ver si hay grupos q tengan 'g' couplings similares).

    * pesar las interacciones con el Z (nro atomico) de c atomo.

    * calcular algun tipo de interaccion entre los atommos: 

        * usando la fuerza nuclear, eletromagnetica ("coulombiana"), o gravitatoria? (COMPARAR 
        ordenes de magnitud!!) Tal vez usar esto como "factor de peso" en las funciones de simetria!

        * usar una "energia de interaccion" (potencial coulombiano) total de las interacciones 
        entre los atomos q participan en el enlace!

    Source:
    https://www.kaggle.com/borisdee/predicting-mulliken-charges-with-acsf-descriptors


[-] usar los "Coulomb matrix" eigenvalues (ver Elton etal, 2018)


[-] usar los `cos1` y `cos2` tmb de Sergio.


[-] usar las distancias C-C (N-C, ò N-N) tmb!


[ ] hacer un histograma de:

    - todas las inter-distancias
    - inter-distancias entre coupled atoms





---
## TEORIA

* eq. 32.19 de Ashcroft (p. 680).



<!--- EOF -->
